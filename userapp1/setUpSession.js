import React, { Component } from 'react';
import {db, auth} from './firebase';
import UserContext from './UserContext';


export const setUpSession = (Component) => {
  class SetUpSession extends Component {
    constructor(props) {
      super(props);

      this.state = {
        authUser: null,

        name: '',
        organization: '',
        prog: '',
        sport: '',
        level: '',

        today: '',
        seasonStart: '',
        weeksUntil: '',

        meso: '',
        week: '',
        thisWeek: {},
        days: [],
        comps: [],

        team: '',
        roster: [],
        type: '',
        loaded: false,

      };
    }

    componentDidMount() {

      auth.onAuthStateChanged(authUser => {
        authUser
          ? (this.state.loaded == false
            ? (this.checkRun(authUser, 50).then(() => this.runAsync(authUser)))
            : this.setState({authUser}))
          : this.reset()
      });
    }


    checkRun(authUser, interval) {
      var retryCount = 0;
      var retryCountLimit = 100;
      const userRef = db.collection('users').doc(authUser.uid);
      var promise = new Promise((resolve, reject) => {
        var timer = setInterval(function () {
          userRef.get().then((doc) => {
            if (doc.exists) {
              console.log('user document exists')
              clearInterval(timer);
              resolve();
              return;
            } else {
              console.log('waiting for document to be created')
            }
          })
          retryCount++;
          if (retryCount >= retryCountLimit) {
              clearInterval(timer);
              reject("retry count exceeded");
          }
        }, interval);
      });

      return promise;
    }


    runAsync(authUser) {
      this.setState({authUser});
      var data = [];
      console.log(authUser);
      const userRef = db.collection('users').doc(authUser.uid);
      userRef.get().then((doc) => {
        if(doc.exists){
          if(doc.data().type == 'coach') {
            console.log('logging in coach');
            console.log('team: ' + doc.data().teamCode)
            this.setState({
              name: doc.data().name,
              organization: doc.data().organization,
              sport: doc.data().sport,
              seasonStart: doc.data().seasonStart,
              type: doc.data().type,
              team: doc.data().teamCode,
            })
            this.fillRoster(doc.data().teamCode);
          } else {
            this.setState({
              seasonStart: doc.data().seasonStart,
              name: doc.data().name,
              organization: doc.data().organization,
              sport: doc.data().sport,
              level: doc.data().level,
              prog: doc.data().program,
              type: doc.data().type,
            })
            data.start = doc.data().seasonStart;
            data.prog = doc.data().program;
          }
        } else {
          console.log('not yet')
        }

      })
      .then(() => {
        console.log('calculating date')
        data.today = this.calcToday()
      })
      .then(() => {
        console.log('type:' + this.state.type)
        if(this.state.type == 'coach') {
          this.setState({loaded: true});
          reject('coach loaded');
        } else {
          data.weeksUntil = this.weeksUntil(data.today, data.start);
        }

      })
      .then(() => {
        console.log(this.state.weeksUntil);
        console.log(this.state.loaded);
        this.getWeek(data.prog, data.weeksUntil)
      })
    }

    fillRoster = (team) => {
      console.log('filling roster of team: ' + team);
      var rost = [];
      db.collection('teams').doc(team).collection('roster').get()
      .then((roster) => {
        roster.forEach((doc) => {
          console.log(doc.data().name)
          rost.push({
            uID: doc.data().uID,
            name: doc.data().name,
          })
          console.log(rost);
        })
        console.log(rost);
        this.setState({roster: rost});
      })
      return
    }
/*
    runAsync(authUser) {
      this.setState({authUser});

      var data = [];
      console.log(authUser);
      const userRef = db.collection('users').doc(authUser.uid);
      userRef.get().then((doc) => {
        if(doc.exists){
          console.log('got it');
          console.log(this.state.loaded);
          this.setState({
            seasonStart: doc.data().seasonStart,
            name: doc.data().name,
            organization: doc.data().organization,
            sport: doc.data().sport,
            level: doc.data().level,
            prog: doc.data().program,
          })
          data.start = doc.data().seasonStart;
          data.prog = doc.data().program;

        } else {
          console.log('not yet')
        }

      })
      .then(() => {

        data.today = this.calcToday()
      })
      .then(() => {
        data.weeksUntil = this.weeksUntil(data.today, data.start);
      })
      .then(() => {
        console.log(this.state.weeksUntil);
        console.log(this.state.loaded);
        this.getWeek(data.prog, data.weeksUntil)
      })
    }
*/
    reset() {
      console.log('Resetting');
      this.setState({
        authUser: null,

        name: '',
        organization: '',
        prog: '',
        sport: '',
        level: '',

        today: '',
        wkOf: '',
        seasonStart: '',
        weeksUntil: '',

        meso: '',
        week: '',
        thisWeek: {},
        days: [],
        comps: [],

        loaded: false
      })
      console.log(this.state);
    }

    calcToday = () => {
      const date = new Date();

      var dd = date.getDate();
      var mm = date.getMonth()+1;
      var yyyy = date.getFullYear();
      if(dd < 10) { dd= '0' + dd };
      if(mm < 10) { mm = '0' + mm };
      const today = mm + '/' + dd + '/' + yyyy;
      this.setState({today});

      const wkOf = this.getMonday(date);
      this.setState({wkOf});

      return today
    }

    getMonday = (date) => {
      var day = date.getDay() || 7;
      if( day !== 1 )
          date.setHours(-24 * (day - 1));
      var dd = date.getDate();
      var mm = date.getMonth()+1;
      var yyyy = date.getFullYear();
      if(dd < 10) { dd= '0' + dd };
      if(mm < 10) { mm = '0' + mm };
      const wkOf = mm + '/' + dd + '/' + yyyy;
      return wkOf;
    }


    weeksUntil = (today, seasonStart) => {
      var weeksUntil;
      if(seasonStart != 'n/a') {
        var td = parseInt(today.substring(3,5), 10);
        var sd = parseInt(seasonStart.substring(3,5), 10);
        var tm = parseInt(today.substring(0,2), 10);
        var sm = parseInt(seasonStart.substring(0,2), 10);
        var ty = parseInt(today.substring(6,10), 10);
        var sy = parseInt(seasonStart.substring(6,10), 10);
        if (sy > ty) {
          sm = sm+12*(sy-ty);
        }
        var daysBetween = (sm-tm)*30 + (sd-td);
        weeksUntil = parseInt((daysBetween)/7);

      } else {
        weeksUntil = 'n/a';
      }
      this.setState({weeksUntil});
      return weeksUntil;
    }

    getWeek = (prog, weeksUntil) => {
      const progRef = db.collection('programs').doc(prog);
      progRef.get().then((doc) => {
        var i;
        var index = 0;
        if (weeksUntil != 'n/a') {
          for (i = 0; i < 52; i++) {
             var iterate = doc.data().weeks[i];
             if(iterate.meso==="Daily Undulating") {
               index = i - parseInt(weeksUntil);
               if (index < 0) {
                 index = index + 52;
                 break;
               }
               break;
             }
          }
        }
        //this.makeCalendar(prog, index);
        const thisW = doc.data().weeks[index];
        this.makeWeek(thisW.days).then((ds) => {
          console.log('made or acquired this week');
          console.log(ds);
          this.setState({
            meso: thisW.meso,
            week: thisW.week,
            days: ds,
          });
          console.log(this.state.days);
          return ds;
        })

      }).then(() => this.setState({loaded: true}));
    }

    makeWeek(days) {
      const user = auth.currentUser;

      var date = this.getMonday(new Date());
      var dateID = date.substring(0,2) + date.substring(3,5) + date.substring(6,10)
      //console.log(dateID);
      const entryID = `${dateID}`;

      weekRef = db.collection('users').doc(user.uid).collection('workouts');
      var newDays = [];
      var inds = [];
      var i;
      var promise = new Promise((resolve) => {
        weekRef.doc(entryID).get().then((doc) => {
          if(doc.exists) {
            console.log('days of this week exist');
            resolve(doc.data().newDays);
          } else {
            for (i = 0; i < days.length; i++) {
              console.log(i);
              if (days[i].day !== undefined) {
                inds.push(i);
                newDays.push({...days[i], complete: false});
                console.log(inds);
              }
            }
            //newDays = {...newDays, complete: 0, ind: inds};
            this.setState({comps: newDays});
            weekRef.doc(entryID).set({newDays: newDays}).then(() => {
              console.log('days of this week made');

              resolve(newDays);
            });
          }
        })
      })

      return promise;

    }

    makeCalendar(prog, index) {
      var calendar = [];
      var weekof = this.getMonday(new Date());
      var dd =  weekof.substring(3,5)
      var mm =  weekof.substring(0,2)
      var yyyy = weekof.substring(6,10)
      var weeksOf = [weekof];

      //var weeksOfFull = false;
      var date = new Date();
      var increment = 0;
      var oneWeekMS = 7 * 24 * 60 * 60 * 1000;
      //var oneWeek = 7;
      while(weeksOf.length < 20) {
        //increment += oneWeekMS;
        date.setTime(date.getTime() + oneWeekMS);
        weeksOf.push(this.getMonday(date));
      }
      const progRef = db.collection('programs').doc(prog);
      progRef.get().then((doc) => {
        for (i=index; i<index+20; i++) {
          var ind = i;
          if (i > 51) {
            ind=ind-52
          }
          var iter = doc.data().weeks[ind];
          calendar.push({
            weekof: weeksOf[i-index],
            meso: iter.meso,
            week: iter.week,
          })
        }
        this.setState({calendar});
      })
    }


    render() {
      return (
        <UserContext.Provider
          value={this.state}>
          <Component />
        </UserContext.Provider>
      );
    }
  }

  return SetUpSession;
}

export default setUpSession;
