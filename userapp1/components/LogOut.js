import React, {Component} from 'react';
import { View, Text, ActivityIndicator, StyleSheet, TouchableOpacity, Dimensions } from 'react-native';
import {auth} from './../firebase';

export default class LogOut extends Component {

  handleLogout = (e) => {
      auth.signOut()
  }

  render() {
    return (
        <TouchableOpacity style={styles.logoutbutton} onPress={this.handleLogout.bind(this)}>
          <Text style={styles.buttontext}> Log Out </Text>
        </TouchableOpacity>
    )
  }
}

const win = Dimensions.get('window');
const drawWid=win.width*.65;
const left = (drawWid-160)/2;

const styles = StyleSheet.create({
  logoutbutton: {
    width: 160,
    height: 40,
    borderRadius: 8,
    justifyContent:'center',
    alignItems: 'center',
    backgroundColor: '#fe1a27',
    padding: 10,
    marginLeft: left,
    marginTop: 25,
  },
  buttontext: {
    fontSize: 18,
    color: 'white',
    fontFamily: 'Montserrat-Regular',
  }
})
