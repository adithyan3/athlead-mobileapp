import React, {Component} from 'react';
import { View, Text, ActivityIndicator, StyleSheet, Button } from 'react-native';


export default class Settings extends Component {

  static navigationOptions: {
    header: 'none'
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.header}>Settings</Text>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: '#c5c6c7',
  },
  text: {
    textAlign: 'center',
    fontSize: 16,
    color: '#3c3c3c',
    fontFamily: 'Montserrat-Regular',
  },
  header: {
    fontSize: 18,
    color: '#263253',
    fontFamily: 'TitilliumWeb-Bold',
  },
})
