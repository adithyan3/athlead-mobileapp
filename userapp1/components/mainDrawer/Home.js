import React, {Component} from 'react';
import { StyleSheet, Platform, Image, Text, View, TouchableOpacity, Dimensions, Button, Modal, Alert, } from 'react-native';
import { Container, Header, Content, Card, CardItem} from 'native-base';
import Record from './workout/Record';
import {auth,db} from './../../firebase';
import setUpSession from './../../setUpSession';


class Home extends Component {
  constructor(props) {
      super(props);

      this.state = {
        //dayIndex: '0',
        //days: this.props.screenProps.days,
        showPreview: false,
        numRend: 0,
      };
    }

  setModalVisible(visible) {
    this.setState({showPreview: visible});
  }

  makeRecord(wID) {
    const user = auth.currentUser;
    this.setModalVisible(!this.state.showPreview);

    var date = this.getMonday(new Date());
    var dateID = date.substring(0,2) + date.substring(3,5) + date.substring(6,10)
    const entryID = `${dateID}`;

    weekRef = db.collection('users').doc(user.uid).collection('workouts').doc(entryID);


    var days = this.props.screenProps.days;
    var newDays = [];

    for (i = 0; i < days.length; i++) {
      if (days[i].wID == wID) {
        newDays.push({...days[i], complete: true, compDate: new Date()});
      } else {
        newDays.push({...days[i]});
      }
    }

    weekRef.set({newDays: newDays}).then(() => {
      this.setState({numRend: 0});

    });
  }

  getMonday = (date) => {
    var day = date.getDay() || 7;
    if( day !== 1 )
        date.setHours(-24 * (day - 1));
    var dd = date.getDate();
    var mm = date.getMonth()+1;
    var yyyy = date.getFullYear();
    if(dd < 10) { dd= '0' + dd };
    if(mm < 10) { mm = '0' + mm };
    const wkOf = mm + '/' + dd + '/' + yyyy;
    return wkOf;
  }

  static navigationOptions = {
    drawerLabel: 'Home',
  };

  render() {

    var wksU = this.props.screenProps.weeksUntil;
    if (parseInt(wksU) < 1) {
      wksU = 'In Season';
    }

    var numRend = 0;

    const dL = this.props.screenProps.days.map((d,i) => {
      if (d.complete == false && numRend < 1) {
        numRend++;
        return (
            <Content>
              <Card key={'top' + i} style={styles.card}>

                <CardItem header style={styles.cardheader}>
                  <Text style={styles.header}>Welcome {this.props.screenProps.name}!</Text>
                </CardItem>

                <CardItem style={styles.cardbody}>
                    <Text style={styles.text}>
                      Today's Date: {this.props.screenProps.today}
                      {'\n'}
                      Weeks Until Season: {wksU}
                    </Text>
                </CardItem>

              </Card>

              <Card key={i} style={styles.card}>

                <CardItem header style={styles.cardheader}>
                  <Text style={styles.header}>
                    Next Workout: {d.wID}
                  </Text>
                </CardItem>

                <CardItem style={styles.cardbody}>
                  <View style={{width: '48%', margin: '1%', alignItems:'center', borderRightWidth:1, borderRightColor: '#3f3f3f'}}>
                    <Text style={styles.text}>
                      {this.props.screenProps.meso}
                    </Text>
                  </View>

                  <View style={{width: '48%', margin: '1%', alignItems:'center'}}>
                    <Text style={styles.text}>
                      Week: {this.props.screenProps.week}
                    </Text>
                  </View>
                </CardItem>

                <CardItem style={styles.cardbody}>

                    <View style={{width: '35%', margin: '1%', justifyContent:'center', alignItems:'center', borderRightWidth:1, borderRightColor: '#3f3f3f'}}>
                      <Text style={styles.text}>
                        Week Of: {this.props.screenProps.wkOf}
                      </Text>
                    </View>

                    <View style={{width: '20%', margin: '1%', alignItems:'center'}}>
                      <Text style={styles.text}>
                        Day {d.day}
                      </Text>
                    </View>

                    <View style={{width: '45%', margin: '1%', padding: 5, alignItems:'center'}}>
                      <TouchableOpacity style={styles.button} onPress={() => this.setModalVisible(!this.state.showPreview)} >
                        <Text style={styles.buttontext}>
                          View Workout
                        </Text>
                      </TouchableOpacity>
                    </View>
                </CardItem>
              </Card>
              <Modal
                animationType="slide"
                transparent={false}
                visible={this.state.showPreview}
                onRequestClose={() => {
                  this.setModalVisible(!this.state.showPreview);
                }}>
                  <Record wID = {d.wID} />
                  <View style={{flexDirection: 'row', justifyContent: 'space-evenly', backgroundColor: '#c5c6c7'}}>
                    <TouchableOpacity style={styles.button} onPress={() => {
                        this.setModalVisible(!this.state.showPreview);
                      }}>
                      <Text style={styles.buttontext}>
                        Close Preview
                      </Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.button} onPress={() => this.makeRecord(d.wID)}>
                      <Text style={styles.buttontext}>
                        Finish Workout
                      </Text>
                    </TouchableOpacity>
                  </View>
              </Modal>
            </Content>
        )
      }
    })

    return (
      <View style = {styles.container}>
        {dL}
      </View>
    )
  }
}

export default Home;

const win = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#c5c6c7',
  },
  card: {
    alignItems: 'center',
    alignSelf: 'center',
    width: win.width-10,
  },
  cardheader: {
    justifyContent: 'center',
    width:'100%',
    backgroundColor:'#bfbfbf',
    borderBottomWidth: 1,
    borderBottomColor: '#3f3f3f',
    borderTopWidth: 1,
    borderTopColor: '#3f3f3f',
    borderRightWidth: 1,
    borderRightColor: '#3f3f3f',
    borderLeftWidth: 1,
    borderLeftColor: '#3f3f3f'
  },
  cardbody: {
    justifyContent: 'center',
    alignItems: 'center',
    width:'100%',
    backgroundColor:'#e1e1e1',
    borderBottomWidth: 1,
    borderBottomColor: '#3f3f3f',
    borderRightWidth: 1,
    borderRightColor: '#3f3f3f',
    borderLeftWidth: 1,
    borderLeftColor: '#3f3f3f'
  },
  text: {
    textAlign: 'center',
    fontSize: 16,
    color: '#3c3c3c',
    fontFamily: 'Montserrat-Regular',
  },
  header: {
    fontSize: 20,
    color: '#263253',
    fontFamily: 'TitilliumWeb-Bold',
  },
  button: {
    width: 160,
    height: 35,
    borderRadius: 8,
    justifyContent:'center',
    alignItems: 'center',
    backgroundColor: '#2c2c2c',
    padding: 10,
  },
  buttontext: {
    fontSize: 16,
    color: 'white',
    fontFamily: 'Montserrat-Regular',
  }
})
