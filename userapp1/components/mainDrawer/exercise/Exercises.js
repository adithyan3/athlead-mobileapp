import React, {Component} from 'react';
import { StyleSheet, Platform, Image, View, TextInput, TouchableOpacity, FlatList } from 'react-native';
import { Left, Right, Container, Header, Content, Item, Input, Icon,
  Button, List, ListItem, Text, Body, Title } from "native-base";
import {auth,db} from './../../../firebase';




export default class Exercises extends Component {
  constructor() {
    super();
    this.state = {
      exercises: [],
      searchVal: '',
      currentExercise: {},
    }
  }

  componentWillMount() {
    const exRef = db.collection('exercises');
    let listing = this.state.exercises;
    exRef.get().then(snap => {
      snap.forEach(function(doc) {

        listing.push(doc.data())
      })
      this.setState({exercises:listing});
    });

  }

  handleSearchVal = (event) => {
    this.setState({ searchVal: event.target.value });
  }

  isSearchValue = (value) => {
    const searchVal = this.state.searchVal.toLowerCase();
    const exerciseValArray = value.toLowerCase().split(' ');
    return  exerciseValArray.some(val => val.startsWith(searchVal));
  }



  renderItem = ({ item }) => {
    return (
      <View style={styles.item} key={item.key}>
        {texts}
      </View>
    )
  }

  renderSeparator = () => {
    return (
      <View
        style={{
          height: 1,
          width: "100%",
          backgroundColor: "#3c3c3c",
        }}
      />
    );
  };

  render () {

    const lis = Array.from(this.state.exercises).filter((val) => this.isSearchValue(val.exName));


    return(
      <Container style={{backgroundColor: '#c5c6c7'}}>
        <Header style={{backgroundColor: '#324151'}} searchBar rounded>
        <Item>
          <Icon name="ios-search" />
          <TextInput style={{width: '90%' }} value={this.state.searchVal} onChangeText={(text) => this.setState({searchVal:text})} placeholder="Search" />
        </Item>
        <Button transparent>
          <Text style={styles.text}>Search</Text>
        </Button>
      </Header>

      <Content>
        <View style={styles.container}>
          <FlatList
            data={lis}
            renderItem={({item}) => (
              <TouchableOpacity onPress={() => this.props.navigation.navigate('ExerciseView',
                {
                  exID: item.exid,
                })}>
                <Text style = {styles.text}>{item.exName}</Text>
              </TouchableOpacity>
            )}
            ItemSeparatorComponent={this.renderSeparator}
          />
        </View>
      </Content>
    </Container>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    backgroundColor: '#c5c6c7',
  },
  text: {
    textAlign: 'left',
    marginLeft: 20,
    paddingTop: 5,
    paddingBottom: 5,
    fontSize: 20,
    color: '#3c3c3c',
    fontFamily: 'Montserrat-Regular',
  },
})
