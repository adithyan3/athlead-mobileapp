import React, {Component} from 'react';
import { StyleSheet, Platform, Image, Text, View, Button, Dimensions } from 'react-native';
import { Container, Header, Content, Card, CardItem} from 'native-base';
import {auth,db} from './../../../firebase';

export default class ExerciseView extends Component {

 constructor(props) {
   super(props);

   this.state = {
     authUser: null,
     exercises: [],
     name: '',
     start: '',
     act: '',
     type: '',
     exID: this.props.exID,

   };

 }



 componentDidMount() {


   const exRef = db.collection('exercises').doc(this.state.exID);
   exRef.get().then((doc) => {
     this.setState({name: doc.data().exName});
     this.setState({start: doc.data().exStart});
     this.setState({act: doc.data().exAct});
     this.setState({type: doc.data().exType});
   });

 }

 render() {
   return (

     <View styles={styles.container}>
       <Card style={styles.card}>
         <CardItem header style={styles.cardheader}>
           <Text style={styles.header}>
              {this.state.name}
            </Text>
          </CardItem>

          <CardItem style={styles.cardbody}>
             <Text style={styles.text}>
               Type: {this.state.type}
            </Text>
          </CardItem>

          <CardItem style={styles.cardbody}>
            <Text style={styles.text}>
              Start: {this.state.start}
           </Text>
          </CardItem>

          <CardItem style={styles.cardbody}>
            <Text style={styles.text}>
              Action: {this.state.act}
            </Text>
          </CardItem>
        </Card>
     </View>
   )
 }
}

const win = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#c5c6c7',
  },
  card: {
    alignSelf: 'center',
    width: win.width-10,
  },
  cardheader: {
    justifyContent: 'center',
    width:'100%',
    backgroundColor:'#bfbfbf',
    borderBottomWidth: 1,
    borderBottomColor: '#3f3f3f',
    borderTopWidth: 1,
    borderTopColor: '#3f3f3f',
    borderRightWidth: 1,
    borderRightColor: '#3f3f3f',
    borderLeftWidth: 1,
    borderLeftColor: '#3f3f3f'
  },
  cardbody: {
    justifyContent: 'center',
    alignItems: 'center',
    width:'100%',
    backgroundColor:'#e1e1e1',
    borderBottomWidth: 1,
    borderBottomColor: '#3f3f3f',
    borderRightWidth: 1,
    borderRightColor: '#3f3f3f',
    borderLeftWidth: 1,
    borderLeftColor: '#3f3f3f'
  },
  text: {
    fontSize: 16,
    color: '#3c3c3c',
    fontFamily: 'Montserrat-Regular',
  },
  header: {
    fontSize: 18,
    color: '#263253',
    fontFamily: 'TitilliumWeb-Bold',
  },
})
