import React, {Component} from 'react';
import { StyleSheet, Platform, Image, Text, View, Button } from 'react-native';
import { Container, Header, Content, Card, CardItem, Body, Title } from 'native-base';
import {auth,db} from './../../../firebase';

class WorkoutHist extends Component {

  constructor(props) {
    super(props);

    this.state = {
      weeks: [],
    }
  }

  static navigationOptions = {
    drawerLabel: 'Settings',
  };

  componentDidMount() {
    const user = auth.currentUser;
    weekRef = db.collection('users').doc(user.uid).collection('workouts');
    wks = [];
    weekRef.get().then((snap) => {
      snap.forEach((doc) => {
        wks.push(doc.data())
      })
    })
    this.setState({weeks: wks});
  }

  render() {

    return (
      <View style={styles.container}>
        <Text style = {styles.header}>
          Workout History - Coming Soon! {'\n'}
        </Text>
        <Text style = {styles.header}>
          Completed Workouts (by Week):
        </Text>
        {this.state.weeks.map((w,i) => {
          return(
            <Text style={styles.text}>{w.wID}</Text>
          )
        })}
      </View>
    )
  }
}

export default WorkoutHist;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: '#c5c6c7',
  },
  text: {
    textAlign: 'center',
    fontSize: 16,
    color: '#3c3c3c',
    fontFamily: 'Montserrat-Regular',
  },
  header: {
    fontSize: 18,
    color: '#263253',
    fontFamily: 'TitilliumWeb-Bold',
  },
})
