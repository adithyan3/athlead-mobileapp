import React, {Component} from 'react';
import { StyleSheet, Text, TextInput, View, Button, TouchableOpacity, Dimensions, Modal } from 'react-native';
import { Container, Header, Content, Picker, /*DatePicker*/} from 'native-base';
import DatePicker from 'react-native-datepicker'
import {auth,db} from './../../../firebase';


class EntryModal extends Component {

  constructor(props) {
    super(props);
    this.state = {
      date: new Date(),
      selected: '',
      height: '',
      weight: '',
    }
  }


  static navigationOptions =  {
    header: 'none'
  };



  makeEntry = () => {
    const {
      date,
      selected,
      weight
    } = this.state;

    const user = auth.currentUser;
    var userID = user.uid;
    userRef = db.collection('users').doc(userID);

    var newEntry = {
      date: date,
      exercise: selected,
      weight: weight,
    }

    var which;

      switch(selected) {
        case 'bench':
          userRef.get().then((doc) => {
            if (doc.data().maxBench == null || weight > doc.data().maxBench) {
              userRef.update({'maxBench': weight});
              which = 'maxBench';

            }
          })
          break;
        case 'squat':
          userRef.get().then((doc) => {
            if (doc.data().maxSquat == null || weight > doc.data().maxSquat) {
              userRef.update({'maxSquat': weight});
              which = 'maxSquat';
            }
          })
          break;
        case 'clean':
          userRef.get().then((doc) => {
            if (doc.data().maxClean == null || weight > doc.data().maxClean) {
              userRef.update({'maxClean': weight});
              which = 'maxClean';
            }
          })
          break;
      }

    var dateID = date.substring(0,2) + date.substring(3,5) + date.substring(6,10)
    console.log(dateID);

    const entryID = `${dateID}${selected}`

    db.collection('users').doc(userID).collection('logs').doc(entryID).set(newEntry)
    .then(() => {
      this.props.update(which, weight);
      this.props.updateList();
      //this.props.toggle();
    })

  }

  render() {

    const {
      date,
      selected,
      weight,
    } = this.state;

    const isInvalid = (
        date === '' ||
        selected === '' || selected == 0 ||
        weight === ''
      ) ;

    const dim = Dimensions.get('window');
    var wid = .85*dim.width;

    return (

      <Modal animationType="slide"
      transparent={true}
      visible={this.props.visible}
      onRequestClose={() => {
        this.props.toggle
      }}>
      <View style={styles.container}>
        <View style={styles.modal}>
          <Text style={styles.header}>Make a Strength Log Entry</Text>


          <DatePicker
            style={{width:150, padding: 10}}
            date={this.state.date}
            mode='date'
            format='MM-DD-YYYY'
            defaultDate={new Date()}
            minDate='01-01-2018'
            maxDate='12-31-2022'
            confirmBtnText="Confirm"
            cancelBtnText="Cancel"
            hideText={false}
            showIcon={false}
            customStyles={{
              dateIcon: {
                position: 'absolute',
                left: 0,
                top: 4,
                marginLeft: 0
              },
              dateInput: {
                borderWidth: 1,
                borderColor: '#2c2c2c',
              }
            }}
            //placeHolderTextStyle={{ color: "#d3d3d3" }}
            onDateChange={date => this.setState({date})}
          />
          <Picker
            selectedValue={this.state.selected}
            style={{ height: 50, width: 200}}
            onValueChange={(itemValue, itemIndex) => this.setState({selected: itemValue})}>
            <Picker.Item key={'unselectable'} label="Select Exercise" value={0} />
            <Picker.Item label="Squat" value="squat" />
            <Picker.Item label="Bench Press" value="bench" />
            <Picker.Item label="Power Clean" value="clean" />
          </Picker>

          <TextInput
            placeholder="Weight (lbs)"
            autoCapitalize="none"
            style={styles.textInput}
            onChangeText={weight => this.setState({ weight })}
            value={this.state.weight}
          />

          <TouchableOpacity disabled={isInvalid} style={styles.button} onPress={this.makeEntry}>
            <Text style={styles.buttontext}>
              Make Entry
            </Text>
          </TouchableOpacity>

          <TouchableOpacity style={styles.button} onPress={this.props.toggle}>
            <Text style={styles.buttontext}>
              Cancel
            </Text>
          </TouchableOpacity>
          </View>
        </View>
      </Modal>

    )
  }
}

export default EntryModal;

const win = Dimensions.get('window');
const wid = (win.width-250)/2;
const ht = (win.height-300)/2

const styles = StyleSheet.create({
  container: {
    flex: 1,
    height: 300,
    width: 250,
    paddingTop: ht,
    paddingLeft: wid,
    //alignItems: 'center',


  },
  modal: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 300,
    width: 250,
    borderRadius: 8,
    borderWidth: 3,
    borderColor: '#2c2c2c',
    backgroundColor: '#e2e2e2',
  },
  textInput: {
    width: 180,
    fontFamily: 'Montserrat-Regular',
    textAlign: 'center',
    marginBottom: 15,
  },
  header: {
    fontSize: 18,
    paddingTop: 10,
    paddingBottom: 2,
    color: '#263253',
    fontFamily: 'TitilliumWeb-Bold',
  },
  button: {
    width: 160,
    height: 35,
    borderRadius: 8,
    justifyContent:'center',
    alignItems: 'center',
    backgroundColor: '#2c2c2c',
    padding: 10,
    marginBottom: 10,
  },
  buttontext: {
    fontSize: 16,
    color: 'white',
    fontFamily: 'Montserrat-Regular',
  }
})

/*<DatePicker
  defaultDate={new Date()}
  minimumDate={new Date(2018, 1, 1)}
  maximumDate={new Date(2022, 12, 31)}
  locale={"en"}
  timeZoneOffsetInMinutes={undefined}
  modalTransparent={false}
  animationType={"fade"}
  androidMode={"default"}
  placeHolderText="Select Entry Date"
  textStyle={{ color: "green" }}
  placeHolderTextStyle={{ color: "#d3d3d3" }}
  onDateChange={date => this.setState({ date })}
/>*/
