import React, {Component} from 'react';
import { StyleSheet, Platform, Image, Text, View, Button, Dimensions, ActivityIndicator, ScrollView,
Modal, TouchableOpacity, FlatList } from 'react-native';
import { Container, Header, Content, Card, CardItem, Left, Body, Right, Title, Accordion, Icon } from 'native-base';
import {auth,db} from './../../../firebase';

import EntryModal from './EntryModal';

const byPropKey = (propertyName, value) => () => ({
  [propertyName]: value,
});

const CHEVRON_DOWN = "chevron-down";
const CHEVRON_UP = "chevron-up";

export default class History extends Component {

  constructor(props) {
    super(props);

    this.state = {

      maxBench: '',
      maxSquat: '',
      maxClean: '',
      benchL: [],
      squatL: [],
      cleanL: [],

      showBench: false,
      showSquat: false,
      showClean: false,
      showModal: false,
    }

    this.toggle = this.toggle.bind(this);
    this.togB = this.togB.bind(this);
    this.togS = this.togS.bind(this);
    this.togC = this.togC.bind(this);
    this.update = this.update.bind(this);
    this.updateList = this.updateList.bind(this);
  }

  toggle() {
    this.setState({showModal: !this.state.showModal});
  }

  togB() {
    this.setState({showBench: !this.state.showBench});
  }

  togS() {
    this.setState({showSquat: !this.state.showSquat});
  }

  togC() {
    this.setState({showClean: !this.state.showClean});
  }

  update(exercise, weight) {
    this.toggle();
    this.setState(byPropKey(exercise, weight));
  }


  updateList() {
    componentDidMount();
  }

  componentDidMount() {
    const user = auth.currentUser;
    userRef = db.collection('users').doc(user.uid);

    var bL = [];
    var sL = [];
    var cL = [];

    userRef.collection('logs').get().then((logs) => {
      logs.forEach((doc) => {
        if (doc.data().exercise === 'bench') {
          bL.push(doc.data())
        }
        if (doc.data().exercise === 'squat') {
          sL.push(doc.data())
        }
        if (doc.data().exercise === 'clean') {
          cL.push(doc.data())
        }

      })

      this.setState({
        benchL: bL,
        squatL: sL,
        cleanL: cL,
      })

    })


    userRef.get().then((doc) => {
      this.setState({
        maxBench: doc.data().maxBench,
        maxSquat: doc.data().maxSquat,
        maxClean: doc.data().maxClean,
      })
    })

  }

  static navigationOptions: {
    header: 'none'
  }


  render() {


    const lifts = [
      {'title': 'Bench Press', 'list': this.state.benchL, 'tog': this.togB, 'open': this.state.showBench},
      {'title': 'Squat', 'list': this.state.squatL, 'tog': this.togS, 'open': this.state.showSquat},
      {'title': 'Power Clean', 'list': this.state.cleanL, 'tog': this.togC, 'open': this.state.showClean},
    ]

    return (
      <View style={styles.container}>
      <ScrollView contentContainerStyle={styles.scroll}>

        <Card style={styles.card}>
          <CardItem style={styles.cardheader}>
            <View style={{width: '55%', margin: '1%', justifyContent:'center', alignItems:'center'}}>
              <Text style={styles.header}>
                1 Rep Maxes (1RM):
              </Text>
            </View>
            <View style={{width: '40%', margin: '1%', padding: 3, alignItems:'center'}}>
              <TouchableOpacity style={styles.button} onPress = {this.toggle}>
                <Text style={styles.buttontext}>
                  New Entry
                </Text>
              </TouchableOpacity>
            </View>
          </CardItem>
          </Card>

          {lifts.map((l,i) => {
            return(
            <Card style={{width: 240, alignItems: 'center'}}>
            <CardItem header button onPress={l.tog} style={styles.onerepcard}>

              <View style={{width: '50%', margin: '1%'}}>
                <Text style={{textAlign: 'center', fontSize: 18,
                color: '#c5c6c7', fontFamily: 'Montserrat-Regular',}}>
                  {l.title}
                </Text>
              </View>

              <View style={{width: '35%', margin: '1%'}}>
              {l.title === 'Bench Press' &&
              <Text style={{textAlign: 'center', fontSize: 18,
              color: '#fe1a27', fontFamily: 'Montserrat-Bold',}}>
                  {this.state.maxBench} (lb)
                </Text>
              }
              {l.title === 'Squat' &&
              <Text style={{textAlign: 'center', fontSize: 18,
              color: '#fe1a27', fontFamily: 'Montserrat-Bold',}}>
                  {this.state.maxSquat} (lb)
                </Text>
              }
              {l.title === 'Power Clean' &&
              <Text style={{textAlign: 'center', fontSize: 18,
              color: '#fe1a27', fontFamily: 'Montserrat-Bold',}}>
                  {this.state.maxClean} (lb)
                </Text>
              }
              </View>

              <View style={{width: '4%', margin: '1%'}}>
                {l.open
                  ? <Icon type="Entypo" style={{ fontSize: 18, color: '#c5c6c7'}} name="chevron-up" />
                  : <Icon type="Entypo" style={{ fontSize: 18, color: '#c5c6c7'}} name="chevron-down" />}
              </View>
          </CardItem>
          {(l.open===true) &&
            <CardItem style={styles.cardbody}>
              <FlatList style={{backgroundColor: '#e2e2e2', width: '100%'}}
                data={l.list}
                renderItem={({item}) => {
                  return (
                  <View style={{flex:1, flexDirection:'row', alignItems: 'center',
                  justifyContent:'space-evenly', borderWidth: 1, borderColor: '#2c2c2c', padding: 10}}>
                      <Text style={styles.text}>
                        {item.date}
                      </Text>
                      <Text style={styles.text}>
                        {item.exercise}
                      </Text>
                      <Text style={styles.text}>
                        {item.weight}
                      </Text>
                  </View>
                )
                }}/>
            </CardItem>
          }
          </Card>

        )}
      )}

    <EntryModal visible={this.state.showModal} toggle={this.toggle} update={this.update} updateList={this.updateList}/>
    </ScrollView>
    </View>

    )
  }
}

/*
if (l.open) {
  return(
  <CardItem style={styles.cardbody}>
  <FlatList
    data={l.list}
    renderItem={({item}) => <Text>{item.date}{item.exercise}</Text>}
  />
  </CardItem>
)
}*/

const win = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: '#c5c6c7',
  },
  scroll: {
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: '#c5c6c7',
  },
  modalContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#c5c6c7',
    width: 300,
    height: 200,
  },
  card: {
    alignItems: 'center',
    alignSelf: 'center',
    width: win.width-10,
  },
  cardheader: {
    justifyContent: 'space-between',
    width:'100%',
    backgroundColor:'#bfbfbf',
    borderBottomWidth: 1,
    borderBottomColor: '#3f3f3f',
    borderTopWidth: 1,
    borderTopColor: '#3f3f3f',
    borderRightWidth: 1,
    borderRightColor: '#3f3f3f',
    borderLeftWidth: 1,
    borderLeftColor: '#3f3f3f'
  },
  cardbody: {
    justifyContent: 'center',
    alignItems: 'center',
    width:'100%',
    backgroundColor:'#e1e1e1',
    borderBottomWidth: 1,
    borderBottomColor: '#3f3f3f',
    borderRightWidth: 1,
    borderRightColor: '#3f3f3f',
    borderLeftWidth: 1,
    borderLeftColor: '#3f3f3f'
  },
  onerepcard: {
    justifyContent: 'space-evenly',
    alignItems: 'center',
    width:'100%',
    height: 60,
    backgroundColor:'#324151',
    borderTopWidth: 1,
    borderTopColor: '#3f3f3f',
    borderBottomWidth: 1,
    borderBottomColor: '#3f3f3f',
    borderRightWidth: 1,
    borderRightColor: '#3f3f3f',
    borderLeftWidth: 1,
    borderLeftColor: '#3f3f3f'
  },
  text: {
    textAlign: 'center',
    fontSize: 16,
    color: '#3c3c3c',
    fontFamily: 'Montserrat-Regular',
  },
  header: {
    fontSize: 20,
    color: '#263253',
    fontFamily: 'TitilliumWeb-Bold',
  },
  button: {
    width: 160,
    height: 35,
    borderRadius: 8,
    justifyContent:'center',
    alignItems: 'center',
    backgroundColor: '#2c2c2c',
    padding: 10,
  },
  buttontext: {
    fontSize: 18,
    color: 'white',
    fontFamily: 'Montserrat-Regular',
  }
})
