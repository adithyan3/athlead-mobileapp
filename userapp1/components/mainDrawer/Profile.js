import React, {Component} from 'react';
import { View, Text, ActivityIndicator, StyleSheet, Dimensions, TouchableOpacity, Button } from 'react-native';
import { Container, Header, Content, Card, CardItem} from 'native-base';
import {auth,db} from './../../firebase'

class Profile extends Component {
  constructor(props) {
    super(props);
  }


  handleLogout = (e) => {
      auth.signOut()
      .then(() => {this.props.navigation.navigate('Welcome')})
  }


  render() {
    const user = auth.currentUser;

    return (
      <View style={styles.container}>
        <Card style={styles.card}>
          <CardItem header style={styles.cardheader}>
            <Text style={styles.header}>
              YOUR PROFILE
            </Text>
          </CardItem>

          <CardItem style={styles.cardbody}>
            <View style={{width: '48%', alignItems: 'flex-start', marginLeft: '2%'}}>
              <Text style={styles.bolder}>Name:</Text>
            </View>
            <View style={{width: '48%', alignItems: 'flex-start'}}>
              <Text style={styles.text}>{this.props.screenProps.name}</Text>
            </View>
          </CardItem>

          <CardItem style={styles.cardbody}>
            <View style={{width: '48%', alignItems: 'flex-start', marginLeft: '2%'}}>
              <Text style={styles.bolder}>Email:</Text>
            </View>
            <View style={{width: '48%', alignItems: 'flex-start'}}>
              <Text style={styles.text}>{user.email}</Text>
            </View>
          </CardItem>

          <CardItem style={styles.cardbody}>
            <View style={{width: '48%', alignItems: 'flex-start', marginLeft: '2%'}}>
              <Text style={styles.bolder}>Organization:</Text>
            </View>
            <View style={{width: '48%', alignItems: 'flex-start'}}>
              <Text style={styles.text}>{this.props.screenProps.organization}</Text>
            </View>
          </CardItem>


          <CardItem style={styles.cardbody}>
            <View style={{width: '48%', alignItems: 'flex-start', marginLeft: '2%'}}>
              <Text style={styles.bolder}>Season Start Date:</Text>
            </View>
            <View style={{width: '48%', alignItems: 'flex-start'}}>
              <Text style={styles.text}>{this.props.screenProps.seasonStart}</Text>
            </View>
          </CardItem>

          <CardItem style={styles.cardbody}>
            <View style={{width: '48%', alignItems: 'flex-start', marginLeft: '2%'}}>
              <Text style={styles.bolder}>Sport:</Text>
            </View>
            <View style={{width: '48%', alignItems: 'flex-start'}}>
              <Text style={styles.text}>{this.props.screenProps.sport}</Text>
            </View>
          </CardItem>

          <CardItem style={styles.cardbody}>
            <View style={{width: '48%', alignItems: 'flex-start', marginLeft: '2%'}}>
              <Text style={styles.bolder}>Level:</Text>
            </View>
            <View style={{width: '48%', alignItems: 'flex-start'}}>
              <Text style={styles.text}>{this.props.screenProps.level}</Text>
            </View>
          </CardItem>

          <CardItem style={styles.cardbody}>
            <View style={{width: '48%', alignItems: 'flex-start', marginLeft: '2%'}}>
              <Text style={styles.bolder}>Your Program ID:</Text>
            </View>
            <View style={{width: '48%', alignItems: 'flex-start'}}>
              <Text style={styles.text}>{this.props.screenProps.prog}</Text>
            </View>
          </CardItem>


          <CardItem style={styles.cardbody}>
            <TouchableOpacity style={styles.logoutbutton} onPress={this.handleLogout.bind(this)}>
              <Text style={styles.buttontext}> Log Out </Text>
            </TouchableOpacity>
          </CardItem>

        </Card>

      </View>
    )
  }
}

export default Profile;

const win = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: '#c5c6c7',
  },
  card: {
    alignSelf: 'center',
    width: win.width-10,
  },
  cardheader: {
    justifyContent: 'center',
    width:'100%',
    backgroundColor:'#bfbfbf',
    borderBottomWidth: 1,
    borderBottomColor: '#3f3f3f',
    borderTopWidth: 1,
    borderTopColor: '#3f3f3f',
    borderRightWidth: 1,
    borderRightColor: '#3f3f3f',
    borderLeftWidth: 1,
    borderLeftColor: '#3f3f3f'
  },
  cardbody: {
    justifyContent: 'center',
    alignItems: 'center',
    width:'100%',
    backgroundColor:'#e1e1e1',
    borderBottomWidth: 1,
    borderBottomColor: '#3f3f3f',
    borderRightWidth: 1,
    borderRightColor: '#3f3f3f',
    borderLeftWidth: 1,
    borderLeftColor: '#3f3f3f'
  },
  listitem: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    width:'100%',
    backgroundColor:'#e1e1e1',
    borderBottomWidth: 1,
    borderBottomColor: '#3f3f3f',
  },
  text: {
    textAlign: 'center',
    fontSize: 16,
    color: '#3c3c3c',
    fontFamily: 'Montserrat-Regular',
  },
  bolder: {
    textAlign: 'center',
    fontSize: 17,
    color: '#263253',
    fontFamily: 'TitilliumWeb-Bold',
  },
  header: {
    fontSize: 20,
    color: '#263253',
    fontFamily: 'TitilliumWeb-Bold',
  },
  logoutbutton: {
    width: 160,
    height: 40,
    borderRadius: 8,
    justifyContent:'center',
    alignItems: 'center',
    backgroundColor: '#fe1a27',
    padding: 10,
  },
  buttontext: {
    fontSize: 18,
    color: 'white',
    fontFamily: 'Montserrat-Regular',
  }
})
