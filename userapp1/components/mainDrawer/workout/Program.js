import React, {Component} from 'react';
import {
  StyleSheet,
  Platform,
  Image,
  Text,
  View,
  Button,
  Modal, FlatList, TouchableOpacity, Dimensions,
} from 'react-native';
import {
  Container,
  Header,
  Content,
  Card,
  CardItem,
  Body,
  Title,
  CheckBox
} from 'native-base';
import Preview from './Preview';
import {auth, db} from './../../../firebase';
import {connect} from 'react-redux';

class HorizFLItem extends Component {


  render() {
    return (
      <View style={{flex:1,flexDirection:'column',alignItems: 'center',justifyContent:'center',width: 100,
            borderRadius: 8, borderWidth: 1, borderColor: 'grey', margin: 2}}>
        <TouchableOpacity onPress={() => {this.props.showWeek(this.props.item.weekof, this.props.item.meso, this.props.item.week)}}
                          style = {{position: 'absolute', top:0, bottom: 0, left: 0, right: 0}}>
          <Text style={{fontSize:12, fontWeight:'bold', color: 'white'}}>
            {this.props.item.weekof}
          </Text>
          <Text style={{fontSize:12, fontWeight:'bold', color: 'white'}}>
            {this.props.item.meso}
          </Text>
          <Text style={{fontSize:12, fontWeight:'bold', color: 'white'}}>
            {this.props.item.week}
          </Text>
        </TouchableOpacity>
      </View>
    )
  }
}

class Program extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showPreview: false,
      wID: '',
      wkOf: this.props.screenProps.wkOf,
      meso: this.props.screenProps.meso,
      week: this.props.screenProps.week,
      days: this.props.screenProps.days,

      //dayIndex: ,
    }
  };

  setModalVisible(visible, wID) {
    this.setState({wID: wID});
    this.setState({showPreview: visible});
  }


  /*showWeek(wkOf, meso, week) {
    const progRef = db.collection('programs').doc(this.props.screenProps.prog);
    progRef.get().then((doc) => {
      var i;
      var index = 0;
      var days = [];
        for (i = 0; i < 52; i++) {
           var iterate = doc.data().weeks[i];
           if(iterate.meso===meso) {
             if (iterate.week===week) {
               this.setState({
                 wkOf: wkOf,
                 meso: meso,
                 week: week,
                 days: iterate.days,
               })
               return
             }
           }
        }
    })
  }*/

  render() {
    const dL = this.props.screenProps.days.map((d, i) => {
      var checked = false;
      //if (d.wID != null) {
        if(d.complete == true) {
          checked = true;
        }
        return(
          <View style={styles.card}>
            <CardItem key={i} style={styles.listitem}>

                <View style={{width: '20%', margin: '1%', alignItems:'center'}}>
                  <CheckBox checked ={checked} color={'#2c2c2c'} />
                </View>

                <View style={{width: '30%', margin: '1%', alignItems:'center'}}>
                  <Text style={styles.text}>
                    Day {d.day}
                  </Text>
                </View>

                <View style={{width: '45%', margin: '1%', alignItems:'center'}}>
                  <TouchableOpacity style={styles.button} onPress={() => this.setModalVisible(!this.state.showPreview, d.wID)}>
                    <Text style={styles.buttontext}>
                      View Workout
                    </Text>
                  </TouchableOpacity>
                </View>
            </CardItem>


            <Modal
              animationType="slide"
              transparent={false}
              visible={this.state.showPreview}
              onRequestClose={() => {
                this.setModalVisible(!this.state.showPreview, null);
              }}>
                <Preview wID = {this.state.wID} />
                <View style={{flexDirection: 'row', justifyContent: 'center', backgroundColor: '#c5c6c7'}}>
                  <TouchableOpacity style={styles.button} onPress={() => {
                      this.setModalVisible(!this.state.showPreview);
                    }}>
                    <Text style={styles.buttontext}>
                      Close Preview
                    </Text>
                  </TouchableOpacity>
                </View>
            </Modal>
          </View>
        )
      //}
    })

    //var showWeek = this.showWeek;

    return (
      <View style = {styles.container}>
        <View style={{flex:1}}>
        <Card key='top' style={styles.card}>

          <CardItem header style={styles.cardheader}>
            <Text style={styles.header}>YOUR PROGRAM</Text>
          </CardItem>

          <CardItem style={styles.cardbody}>
              <Text style={styles.text}>
                {this.props.screenProps.sport}
                {'\n'}
                Level: {this.props.screenProps.level}
              </Text>
          </CardItem>
        </Card>

        <Card key='week' style = {styles.card}>
          <CardItem header style={styles.cardheader}>
            <Text style={styles.header}>
              Week Of: {this.props.screenProps.wkOf}
            </Text>
          </CardItem>

          <CardItem style={styles.cardbody}>
            <View style={{width: '48%', margin: '1%', alignItems:'center', borderRightWidth:1, borderRightColor: '#3f3f3f'}}>
              <Text style={styles.text}>
                {this.props.screenProps.meso}
              </Text>
            </View>

            <View style={{width: '48%', margin: '1%', alignItems:'center'}}>
              <Text style={styles.text}>
                Week: {this.props.screenProps.week}
              </Text>
            </View>
          </CardItem>

          {dL}
        </Card>
      </View>

      <View style={{height: 100}}>
        <FlatList style={{backgroundColor: 'black', opacity: 0.5}}
          horizontal={true}
          data={this.props.screenProps.calendar}
          renderItem={({item, index}) => {
            return (
              <HorizFLItem item={item} index={index} parentFlatList={this} showWeek={showWeek.bind(this)}/>
            )
          }} />
      </View>
    </View>
  )
}
}

export default Program;

const win = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: '#c5c6c7',
  },
  card: {
    alignSelf: 'center',
    width: win.width-10,
    
  },
  cardheader: {
    justifyContent: 'center',
    width:'100%',
    backgroundColor:'#bfbfbf',
    borderBottomWidth: 1,
    borderBottomColor: '#3f3f3f',
    borderTopWidth: 1,
    borderTopColor: '#3f3f3f',
    borderRightWidth: 1,
    borderRightColor: '#3f3f3f',
    borderLeftWidth: 1,
    borderLeftColor: '#3f3f3f'
  },
  cardbody: {
    justifyContent: 'center',
    alignItems: 'center',
    width:'100%',
    backgroundColor:'#e1e1e1',
    borderBottomWidth: 1,
    borderBottomColor: '#3f3f3f',
    borderRightWidth: 1,
    borderRightColor: '#3f3f3f',
    borderLeftWidth: 1,
    borderLeftColor: '#3f3f3f'
  },
  listitem: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    width:'100%',
    backgroundColor:'#e1e1e1',
    borderBottomWidth: 1,
    borderBottomColor: '#3f3f3f',
    borderRightWidth: 1,
    borderRightColor: '#3f3f3f',
    borderLeftWidth: 1,
    borderLeftColor: '#3f3f3f'
  },
  text: {
    textAlign: 'center',
    fontSize: 16,
    color: '#3c3c3c',
    fontFamily: 'Montserrat-Regular',
  },
  header: {
    fontSize: 20,
    color: '#263253',
    fontFamily: 'TitilliumWeb-Bold',
  },
  button: {
    width: 160,
    height: 35,
    borderRadius: 8,
    justifyContent:'center',
    alignItems: 'center',
    backgroundColor: '#2c2c2c',
    padding: 10,
  },
  buttontext: {
    fontSize: 16,
    color: 'white',
    fontFamily: 'Montserrat-Regular',
  }
})
