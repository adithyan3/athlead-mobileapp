import React, {Component} from 'react';
import {
  StyleSheet,
  Platform,
  Image,
  Text,
  View,
  Button,
  Modal, FlatList, TouchableOpacity
} from 'react-native';
import {
  Container,
  Header,
  Content,
  Card,
  CardItem,
  Body,
  Title,
  CheckBox
} from 'native-base';
import Preview from './Preview';
import {auth, db} from './../../../firebase';
import {connect} from 'react-redux';

class HorizFLItem extends Component {
  render() {
    return (
      <View style={{flex:1,flexDirection:'column',alignItems: 'center',justifyContent:'center',width: 100,
            borderRadius: 8, borderWidth: 1, borderColor: 'grey', margin: 2}}>
        <TouchableOpacity onPress={() => {alert('Week Of' + this.props.item.weekof)}}
                          style = {{position: 'absolute', top:0, bottom: 0, left: 0, right: 0}}/>
        <Text style={{fontSize:12, fontWeight:'bold', color: 'white'}}>
          {this.props.item.weekof}
        </Text>
        <Text style={{fontSize:12, fontWeight:'bold', color: 'white'}}>
          {this.props.item.meso}
        </Text>
        <Text style={{fontSize:12, fontWeight:'bold', color: 'white'}}>
          {this.props.item.week}
        </Text>
      </View>
    )
  }
}

class Program extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showPreview: false,
      wID: '',
    }
  };

  setModalVisible(visible, wID) {
    this.setState({wID: wID});
    this.setState({showPreview: visible});
  }

  render() {

    const dL = this.props.screenProps.days.map((d, i) => {
      if (d.wID != null) {
        return (
          <Content key={i}>
            <Card key={i} style = {styles.card}>
            <CardItem>
              <Text>
                Day {i + 1}
                {': '}
                {d.wID}
              </Text>
            </CardItem>
            <CardItem>
              <Button title="View Workout" onPress={() => this.setModalVisible(!this.state.showPreview, d.wID)}></Button>
            </CardItem>
            <CardItem>
              <CheckBox checked ={false} color={'blue'} />
            </CardItem>
          </Card>
          <Modal
          animationType="slide"
          transparent={false}
          visible={this.state.showPreview}
          onRequestClose={() => {
            this.setModalVisible(!this.state.showPreview, null);
          }}>
            <Preview wID = {this.state.wID} />
            <Button title = 'Close Preview' onPress={() => {
                this.setModalVisible(!this.state.showPreview, null);
              }} />
          </Modal>
      </Content>)
      }

    })

    var wksU = this.props.screenProps.weeksUntil;
    if (parseInt(wksU) < 1) {
      wksU = 'In Season';
    }

    return (
      <View style = {styles.container}>
          <Text style={styles.text}>
            Sport: {this.props.screenProps.sport}
            {'\n'}
            Level: {this.props.screenProps.level}
            {'\n'}{'\n'}
            Today's Date: {this.props.screenProps.today}
            {'\n'}
            Week Of: {this.props.screenProps.wkOf}
            {'\n'}
            Weeks Until Season: {wksU}
            {'\n'}{'\n'}
            Schedule for this week:
            {'\n'}
            Mesocycle: {this.props.screenProps.meso}
            {'\n'}
            Week: {this.props.screenProps.week}
            {'\n'}
          </Text>
          {dL}
        <View style={{height: 100}}>
          <FlatList style={{backgroundColor: 'black', opacity: 0.5}}
            horizontal={true}
            data={this.props.screenProps.calendar}
            renderItem={({item, index}) => {
              return (
                <HorizFLItem item={item} index={index} parentFlatList={this} />
              )
            }} />
        </View>
      </View>
    )
  }

}
export default Program;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#c5c6c7',
  },
  card: {
    alignItems: 'center',
    width: 400,
  },
  cardheader: {
    justifyContent: 'center',
    width:'100%',
    backgroundColor:'#bfbfbf',
    borderBottomWidth: 1,
    borderBottomColor: '#3f3f3f',
    borderTopWidth: 1,
    borderTopColor: '#3f3f3f'
  },
  cardbody: {
    justifyContent: 'center',
    alignItems: 'center',
    width:'100%',
    backgroundColor:'#e1e1e1',
    borderBottomWidth: 1,
    borderBottomColor: '#3f3f3f',
  },
  text: {
    textAlign: 'center',
    fontSize: 17,
    color: '#3c3c3c'
  },
  header: {
    fontSize: 17,
    fontWeight: 'bold',
    color: '#263253',
  },
  button: {
    height: 40,
    borderRadius: 8,
    justifyContent:'center',
    alignItems: 'center',
    backgroundColor: '#2f2f2f',
    padding: 1,
  },
  buttontext: {
    fontSize: 16,
    color: 'white'
  }
})
