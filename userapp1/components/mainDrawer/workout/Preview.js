import React, {Component} from 'react';
import { StyleSheet, Platform, Image, Text, View, Button, Dimensions, ActivityIndicator } from 'react-native';
import { Container, Header, Content, Card, CardItem, Body, Title } from 'native-base';
import {auth,db} from './../../../firebase';
import ExerciseCard from './ExerciseCard'

var typeEnum = {
  WARM_UP: "Warm Up",
  CORE_LIFT: "Core Lift"
}

export default class Preview extends Component {

constructor(props) {
  super(props);

  this.state = {
   authUser: null,
   exercises: [],
   exerciseMap: {},
   day: '',
   level: '',
   week: '',
   meso: '',
   wkID: this.props.wID,
   loadedCues: false,
   loadedExs: false,
  };
}

componentDidMount() {
  //const user = auth.currentUser;
  //this.setState({ authUser: user });

  const cueRefs = db.collection('exercises');
  cueRefs.get().then((snapshot) => {
    var exerciseMap = {};
    snapshot.docs.map((doc) =>{
      var data = doc.data();
      exerciseMap[data.exid] = data;
    })
    this.setState({exerciseMap: exerciseMap, loadedCues: true})
  });

  const workRef = db.collection('workouts').doc(this.state.wkID);
  workRef.get().then((doc) => {
    this.setState({exercises: doc.data().exercises});
    this.setState({day: doc.data().day});
    this.setState({week: doc.data().week});
    this.setState({level: doc.data().level});
    this.setState({meso: doc.data().meso});
  }).then(() => {
    this.setState({loadedExs: true});
  });
}

static navigationOptions = {
  drawerLabel: 'Todays workout',
};


  getCues(exid){
    console.log(this.state.exerciseMap[exid])
    return this.state.exerciseMap[exid];
  }

  generateExerciseCard(ex, warmUp, key) {
    var compare = typeEnum.CORE_LIFT;
    if(warmUp) compare = typeEnum.WARM_UP;
    if(ex.type != compare) return;
    var defaultCues = {exStart: "Loading..."}
    var cues = defaultCues;
    return (
      <ExerciseCard exercise = {ex} key= {key} loadedCues = {this.state.loadedCues} getCues = {this.getCues.bind(this)}/>
    )
  }


  render () {

    const exList = this.state.exercises;

    var key = 0;

    if (this.state.loadedExs === false) {
      return(
        <View style={styles.container}>
          <ActivityIndicator size='large' color='#263253' />
        </View>
      )
    } else {
      return (
        <View style = {styles.container}>
          <Content>
            <Card style={styles.card}>
              <CardItem header style={styles.cardheader}>
                <Text style={styles.header}>
                  {this.state.wkID}
                </Text>
              </CardItem>
              <CardItem style={styles.cardbody}>
                  <Text style={styles.text}>
                    {this.state.meso}{'\n'}
                    Week: {this.state.week}{'\n'}
                    Day {this.state.day}
                  </Text>
              </CardItem>
            </Card>

            <Text style={styles.header}>
              Warm Up:
            </Text>
            {exList.map((ex) => {
              key++;
              return(
                this.generateExerciseCard(ex,true,key)
              )
            })}

            <Text style={styles.header}>
              Core Lifts:
            </Text>
            {exList.map((ex) => {
              key++;
              return(
                this.generateExerciseCard(ex,false,key)
              )
            })}


          </Content>
        </View>
      )
    }

  }
}

const win = Dimensions.get('window')

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignSelf: 'stretch',
    width: win.width,
    height: win.height,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#c5c6c7',
  },
  card: {
    justifyContent: 'center',
    alignSelf: 'center',
    width: win.width-10,
  },
  cardheader: {
    justifyContent: 'center',
    width:'100%',
    backgroundColor:'#bfbfbf',
    borderBottomWidth: 1,
    borderBottomColor: '#3f3f3f',
    borderTopWidth: 1,
    borderTopColor: '#3f3f3f',
    borderRightWidth: 1,
    borderRightColor: '#3f3f3f',
    borderLeftWidth: 1,
    borderLeftColor: '#3f3f3f'
  },
  cardbody: {
    justifyContent: 'center',
    alignItems: 'center',
    width:'100%',
    backgroundColor:'#e1e1e1',
    borderBottomWidth: 1,
    borderBottomColor: '#3f3f3f',
    borderRightWidth: 1,
    borderRightColor: '#3f3f3f',
    borderLeftWidth: 1,
    borderLeftColor: '#3f3f3f'
  },
  text: {
    textAlign: 'center',
    fontSize: 16,
    color: '#3c3c3c',
    fontFamily: 'Montserrat-Regular',
  },
  header: {
    fontSize: 18,
    color: '#263253',
    fontFamily: 'TitilliumWeb-Bold',
  },
})
