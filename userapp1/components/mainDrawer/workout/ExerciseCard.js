import React, {Component} from 'react';
import { StyleSheet, Platform, Image, Text, TextInput, View, Dimensions } from 'react-native';
import { Container, Header, Content, Card, CardItem, Body, Title, Left, Right, Icon, Button} from 'native-base';
import {auth,db} from './../../../firebase';

const unitEnum = {
  WEIGHT: "Weight",
  TIME: "Time",
  DISTANCE: "Distance",
  OTHER: "Other"
}

const weightTypeEnum = {
  FIXED_WT: "Fixed Weight",
  BODY_WT: "Body Weight",
  PC: "Percentage",
}

const cueTypeEnum = {
  START: 0,
  ACTION: 1,
  FINISH: 2
}

const FIXED_WT_TEMPLATE = (ex) =>
`${ex.sets}x${ex.reps} reps @ ${ex.fweight} lbs`;

const BODY_WT_TEMPLATE = (ex) =>
`${ex.sets}x${ex.reps} reps @ bodyweight`;

const PC_TEMPLATE_1 = (ex) =>
`${ex.sets}x${ex.reps} reps`;

const PC_TEMPLATE_2 = (ex) =>
`% per set: ${ex.perc}`;

const TIME_TEMPLATE = (ex) =>
`${ex.sets}x${ex.time} seconds`;

const DISTANCE_TEMPLATE = (ex) =>
`${ex.sets}x${ex.dist} meters`;

const OTHER_TEMPLATE = (ex) =>
`${ex.sets}x${ex.descrip}`;

const CHEVRON_DOWN = "chevron-down";
const CHEVRON_UP = "chevron-up";
const LT_BLUE = "#E5F8FF";


export default class ExerciseCard extends Component {

  constructor(props){
    super(props)
    this.state = {
      open: false,

    }
  }


  openClose(){
    this.setState({open: !this.state.open})
  }



  getIconName(){
    var ret = CHEVRON_DOWN;
    if(this.state.open) ret = CHEVRON_UP;
    return ret;
  }

  getRepText(ex){
    switch(ex.unit){
      case unitEnum.WEIGHT:
        switch(ex.wtType){
          case weightTypeEnum.FIXED_WT:
            return FIXED_WT_TEMPLATE(ex);
            break;
          case weightTypeEnum.BODY_WT:
            return BODY_WT_TEMPLATE(ex);
            break;
          case weightTypeEnum.PC:
            return PC_TEMPLATE_1(ex);
            break;
        }
        break;
      case unitEnum.TIME:
        return TIME_TEMPLATE(ex);
        break;
      case unitEnum.DISTANCE:
        return DISTANCE_TEMPLATE(ex);
        break;
      case unitEnum.OTHER:
        return OTHER_TEMPLATE(ex);
        break;
    }
  }


  generateCue(type, content){
    var header = ""
    if(type == cueTypeEnum.START) header = "Start:"
    if(type == cueTypeEnum.ACTION) header = "Action:"
    if(type == cueTypeEnum.FINISH) header = "Finish:"
    return(
      <CardItem style={{...styles.cardbody, backgroundColor: LT_BLUE}} key = {type}>
        <Text style = {styles.text}>{header}  </Text>
        <Text style={{...styles.text, paddingRight: 8}}>{content}</Text>
      </CardItem>
    )
  }

  getCoachingCues(){
    var retArr = []
    var cues = this.props.getCues(this.props.exercise.exid);
    if(cues === undefined) {
      var loading = (
        <CardItem style={{...styles.cardbody, backgroundColor: LT_BLUE}}>
          <Text style={{fontWeight: 'bold'}}>Loading...</Text>
        </CardItem>
      )
      retArr.push(loading)
      return retArr;
    }
    if("exStart" in cues) retArr.push(this.generateCue(cueTypeEnum.START,cues.exStart))
    if("exAct" in cues) retArr.push(this.generateCue(cueTypeEnum.ACTION,cues.exAct))
    if("exFin" in cues) retArr.push(this.generateCue(cueTypeEnum.FINISH,cues.exFin))

    return retArr;
  }

  render () {
    return(
      <Card>
        <CardItem header style={{borderBottomWidth: 2, borderTopWidth: 0, backgroundColor: LT_BLUE}} >
          <Left>
            <Text style={styles.header}>
              {this.props.exercise.exName}
            </Text>
          </Left>
          <Right>
            <Text style={styles.text}>
              {this.getRepText(this.props.exercise)}
            </Text>
          </Right>
        </CardItem>
        {this.state.entryOpen && this.getFields(this.props.exercise)}
        {(this.props.exercise.unit == unitEnum.WEIGHT && this.props.exercise.wtType == weightTypeEnum.PC) && (
          <Body>
            <CardItem>
              <Text style={styles.text}>{PC_TEMPLATE_2(this.props.exercise)}</Text>
            </CardItem>
          </Body>
        )}
        <CardItem footer bordered button onPress={this.openClose.bind(this)} style={{borderBottomWidth: 0, borderTopWidth: 2, backgroundColor: LT_BLUE}}>
        <Left>
          <Text style={styles.text}>
            Coaching Cues
          </Text>
        </Left>
        <Right>
          <Icon type="Entypo" name={this.getIconName()}/>
        </Right>
        </CardItem>
        {this.state.open && this.getCoachingCues()}
      </Card>
    )
  }
}

const win = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignSelf: 'stretch',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#c5c6c7',
  },
  card: {
    justifyContent: 'center',
    alignSelf: 'center',
    width: win.width-10,
  },
  cardheader: {
    justifyContent: 'center',
    width:'100%',
    backgroundColor:'#bfbfbf',
    borderBottomWidth: 1,
    borderBottomColor: '#3f3f3f',
    borderTopWidth: 1,
    borderTopColor: '#3f3f3f',
    borderRightWidth: 1,
    borderRightColor: '#3f3f3f',
    borderLeftWidth: 1,
    borderLeftColor: '#3f3f3f'
  },
  cardbody: {
    justifyContent: 'center',
    alignItems: 'center',
    width:'100%',
    backgroundColor:'#e1e1e1',
    borderBottomWidth: 1,
    borderBottomColor: '#3f3f3f',
    borderRightWidth: 1,
    borderRightColor: '#3f3f3f',
    borderLeftWidth: 1,
    borderLeftColor: '#3f3f3f'
  },
  text: {
    textAlign: 'center',
    fontSize: 15,
    color: '#3c3c3c',
    fontFamily: 'Montserrat-Regular',
  },
  header: {
    fontSize: 16,
    color: '#263253',
    fontFamily: 'TitilliumWeb-Bold',
  },
})
