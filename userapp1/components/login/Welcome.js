import React, {Component} from 'react';
import { View, Text, ActivityIndicator, StyleSheet, Button, Image, TouchableOpacity } from 'react-native';


export default class Welcome extends Component {
/*
  constructor(props) {
    super(props);

    this.state = {
      show: false,
    }
  }

  componentWillMount() {
    this.setState({show: false})
  }
  componentDidMount() {

    setTimeout(() => {this.setState({show: true})}, 1600);
  }

  componentWillUnmount() {
    this.setState({show: false});
  }

  static navigationOptions: {
    header: 'none'
  }*/

  render() {

    return (
      <View style={styles.container}>
        <View style={{ height: 240, width: 300, backgroundColor: '#c5c6c7',
          alignItems: 'center', justifyContent: 'center', marginTop: 100}}>
          <Image source={require('./../mainDrawer/logo2.png')}
          style={{height: 180, width:220, borderRadius: 0}} />
        </View>
        <Text style = {styles.greeting}>
          Welcome!
        </Text>

          <View>
            <TouchableOpacity style = {styles.button} onPress={()=>this.props.navigation.navigate('Login')}>
              <Text style={styles.buttontext}>
                Log in
              </Text>
            </TouchableOpacity>
            <TouchableOpacity style = {styles.button}onPress={()=>this.props.navigation.navigate('Register')}>
              <Text style={styles.buttontext}>
                Register
              </Text>
            </TouchableOpacity>
          </View>


      </View>
    )
  }
}
const styles = StyleSheet.create({
  container: {
    //marginTop: '100px',
    flex: 1,
    //justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#c5c6c7'
  },
  greeting: {
    fontSize: 32,
    color: '#263253',
    fontFamily: 'Montserrat-Regular',
    paddingBottom: 50,
  },
  button: {
    width: 160,
    height: 40,
    borderRadius: 8,
    justifyContent:'center',
    alignItems: 'center',
    backgroundColor: '#2c2c2c',
    padding: 10,
    marginBottom: 8,
  },
  buttontext: {
    fontSize: 18,
    color: 'white',
    fontFamily: 'Montserrat-Regular',
  }
})
