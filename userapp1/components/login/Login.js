import React, {Component} from 'react';
import { StyleSheet, Text, TextInput, View, Button, TouchableOpacity } from 'react-native';
import {auth} from './../../firebase';

const byPropKey = (propertyName, value) => () => ({
  [propertyName]: value,
});


class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: '',
      password: '',
      error: null,
    };
  }



  handleLogin = (e) => {
    const {
      email,
      password,
    } = this.state;

    e.preventDefault();
    auth.signInWithEmailAndPassword(email, password)
      .then(() => {
        this.setState({
          email: '',
          password: '',
          error: null,
        });
        //this.props.navigation.navigate('DrawNav');
      })
      .catch(error => {
        this.setState(byPropKey('error', error));
      });


  }




  render() {
    const {
      email,
      password,
      error,
    } = this.state;

    const isInvalid =
      password === '' ||
      email === '';

    return (
      <View style={styles.container}>
        <Text style = {styles.greeting}>Login</Text>
        {error &&
          <Text style={{ color: 'red' }}>
            {error.message}}
          </Text>}
        <TextInput
          style={styles.textInput}
          autoCapitalize="none"
          placeholder="Email"
          onChangeText={email => this.setState({ email })}
          value={email}
        />
        <TextInput
          secureTextEntry
          style={styles.textInput2}
          autoCapitalize="none"
          placeholder="Password"
          onChangeText={password => this.setState({ password })}
          value={password}
        />
        <TouchableOpacity disabled={isInvalid} style = {styles.buttonL} onPress={(e)=>this.handleLogin(e)}>
          <Text style={styles.buttontext}>
            Log in
          </Text>
        </TouchableOpacity>
        <Text style={styles.text}>Don't have an account?</Text>
        <TouchableOpacity style = {styles.buttonR} onPress={()=>this.props.navigation.navigate('Register')}>
          <Text style={styles.buttontext}>
            Register
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style = {styles.button1} onPress={()=>this.props.navigation.navigate('ForgotPass')}>
          <Text style={styles.buttontext}>
            Forgot Password?
          </Text>
        </TouchableOpacity>
      </View>

    )
  }
}

export default Login

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#c5c6c7'
  },
  textInput: {
    height: 40,
    width: '80%',
    borderColor: '#2c2c2c',
    borderWidth: 1,
    marginTop: 8,
  },
  textInput2: {
    height: 40,
    width: '80%',
    borderColor: '#2c2c2c',
    borderWidth: 1,
    marginTop: 8,
    marginBottom: 20,
  },
  greeting: {
    fontSize: 32,
    color: '#263253',
    fontFamily: 'Montserrat-Regular',
    paddingBottom: 50,
  },
  text: {
    textAlign: 'center',
    fontSize: 16,
    color: '#3c3c3c',
    fontFamily: 'Montserrat-Regular',
    marginBottom: 5
  },
  buttonL: {
    width: 160,
    height: 35,
    borderRadius: 8,
    justifyContent:'center',
    alignItems: 'center',
    backgroundColor: '#2c2c2c',
    padding: 10,
    marginBottom: 30,
  },
  buttonR: {
    width: 160,
    height: 35,
    borderRadius: 8,
    justifyContent:'center',
    alignItems: 'center',
    backgroundColor: '#2c2c2c',
    padding: 10,
    marginBottom: 10,
  },
  button1: {
    width: 200,
    height: 35,
    borderRadius: 8,
    justifyContent:'center',
    alignItems: 'center',
    backgroundColor: '#2c2c2c',
    padding: 10,
    marginBottom: 8,
  },
  buttontext: {
    fontSize: 16,
    color: 'white',
    fontFamily: 'Montserrat-Regular',
  }
})
