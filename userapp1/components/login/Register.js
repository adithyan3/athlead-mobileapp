import React, {Component} from 'react';
import { StyleSheet, Text, TextInput, View, Button } from 'react-native';
import {auth, db} from './../../firebase';

export default class Register extends Component {
  render () {
    return (
      <View style = {styles.container}>
        <Text style = {{fontSize: 24}}>Register as: </Text>

        <View style = {styles.button}>
          <Button
            title="An athlete on a team"
            onPress={() => this.props.navigation.navigate('RegisterTeamAth')}
          />
        </View>

        <View style = {styles.button}>
          <Button
            title="An individual athlete"
            onPress={() => this.props.navigation.navigate('RegisterIndivAth')}
          />
        </View>

        <View style = {styles.button}>
          <Button
            title="A Coach"
            onPress={() => this.props.navigation.navigate('RegisterCoach')}
          />
        </View>

        <View style= {styles.loginbutton}>
          <Button
            title="Already have an account? Login "
            onPress={() => this.props.navigation.navigate('Login')}
          />
        </View>

      </View>
    )
  }
}




const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  textInput: {
    height: 40,
    width: '90%',
    borderColor: 'gray',
    borderWidth: 1,
    marginTop: 8
  },
  button: {
    paddingTop: 8
  },
  loginbutton: {
    paddingTop: 30
  }
})
