import React, {Component} from 'react';
import { StyleSheet, Text, TextInput, View, Button } from 'react-native';
import {auth} from './../../firebase';

const byPropKey = (propertyName, value) => () => ({
  [propertyName]: value,
});


class ForgotPass extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      error: null,
    };
  }

  static navigationOptions: {
    header: 'none'
  }

  passReset = (e) => {
    const { email } = this.state;

    auth.sendPasswordResetEmail(email)
      .then(() => {
        this.setState({
          email: '',
          error: null,
        });
        this.props.navigation.navigate('Welcome');
      })
      .catch(error => {
        this.setState(byPropKey('error', error));
      });

    e.preventDefault();
  }

  render() {
    const {
      email,
      error,
    } = this.state;

    const isInvalid = email === '';

    return (
      <View style={styles.container}>
        <Text>Reset Password</Text>
        {error &&
          <Text style={{ color: 'red' }}>
            {error.message}}
          </Text>}
        <TextInput
          style={styles.textInput}
          autoCapitalize="none"
          placeholder="Email"
          onChangeText={email => this.setState({ email })}
          value={email}
        />

        <Button disabled={isInvalid} title="Send Reset Email " onPress={this.passReset} />
      </View>
    )
  }
}

export default ForgotPass;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  }
})
