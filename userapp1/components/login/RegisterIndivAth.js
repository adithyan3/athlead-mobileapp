import React, {Component} from 'react';
import { StyleSheet, Text, TextInput, View, Button, Picker } from 'react-native';
import {auth, db} from './../../firebase';

const byPropKey = (propertyName, value) => () => ({
  [propertyName]: value,
});


class RegisterIndivAth extends Component {
  constructor(props) {
    super(props);

    this.state = {
      username: '',
      email: '',
      passwordOne: '',
      passwordTwo: '',
      sport: '',
      level: '',
      error: null,
    };

  }

  createActDB(name, email, sport, level, id) {

    var prog = '';
    db.collection('programs').where('sport', '==', sport).where('level', '==', level)
    .get()
    .then((doc) => {
      doc.forEach((p) => {
        prog = p.data().pid;
      })
      const newUser = {
        name: name,
        email: email,
        organization: 'None',
        seasonStart: 'n/a',
        sport: sport,
        level: level,
        program: prog,
        type: 'individual',
        regDate: new Date(),
      }
      console.log(prog);
      db.collection('users').doc(id).set(newUser)
      .then(() => {
        console.log('new user added');
      });
    });
  };



  handleReg = (e) => {

    const {
      username,
      email,
      passwordOne,
      sport,
      level,
    } = this.state;



    auth.createUserWithEmailAndPassword(email, passwordOne)
      .then((authUser) => {
        this.createActDB(username, email, sport, level, authUser.user.uid);
        this.setState({
          username: '',
          email: '',
          passwordOne: '',
          passwordTwo: '',
          error: null,
        });

      })
      .catch(error => {
        this.setState(byPropKey('error', error));
      });

      e.preventDefault();
  };


  render() {
    const {
      username,
      email,
      passwordOne,
      passwordTwo,
      sport,
      level,
      error,
    } = this.state;

    const isInvalid =
        passwordOne !== passwordTwo ||
        passwordOne === '' ||
        email === '' ||
        username === '' ||
        sport === '' ||
        level === '';

    return (
      <View style={styles.container}>
        <Text style = {{fontSize: 20}}>Register as an individual athlete</Text>
        {error &&
          <Text style={{ color: 'red' }}>
            {error.message}}
          </Text>}
        <TextInput
          placeholder="Full Name"
          autoCapitalize="none"
          style={styles.textInput}
          onChangeText={username => this.setState({ username })}
          value={username}
        />
        <TextInput
          placeholder="Email"
          autoCapitalize="none"
          style={styles.textInput}
          onChangeText={email => this.setState({ email })}
          value={email}
        />
        <TextInput
          secureTextEntry
          placeholder="Password"
          autoCapitalize="none"
          style={styles.textInput}
          onChangeText={passwordOne => this.setState({ passwordOne })}
          value={passwordOne}
        />
        <TextInput
          secureTextEntry
          placeholder="Confirm Password"
          autoCapitalize="none"
          style={styles.textInput}
          onChangeText={passwordTwo => this.setState({ passwordTwo })}
          value={passwordTwo}
        />
        <Picker
          selectedValue={this.state.sport}
          style={{ height: 50, width: 375 }}
          onValueChange={sport => this.setState({ sport })}>
          <Picker.Item label="Basketball" value="Basketball" />
          <Picker.Item label="Football" value="Football" />
          <Picker.Item label="Lacrosse" value="Lacrosse" />
          <Picker.Item label="Soccer" value="Soccer" />
          <Picker.Item label="Volleyball" value="Volleyball" />
          <Picker.Item label="Wrestling" value="Wrestling" />
        </Picker>
        <Picker
          selectedValue={level}
          style={{ height: 50, width: 175 }}
          onValueChange={level => this.setState({ level })}>
          <Picker.Item label="0" value="0" />
          <Picker.Item label="1" value="1" />
          <Picker.Item label="2" value="2" />
        </Picker>

        <View style = {styles.button}>
          <Button disabled={isInvalid} title="Register " onPress={this.handleReg} />
        </View>
        <View style = {styles.button}>
          <Button
            title="Already have an account? Login "
            onPress={() => this.props.navigation.navigate('Login')}
          />
        </View>


      </View>
    )
  }
}

export default RegisterIndivAth

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  textInput: {
    height: 40,
    width: '90%',
    borderColor: 'gray',
    borderWidth: 1,
    marginTop: 8
  },
  button: {
    paddingTop: 8
  }
})
