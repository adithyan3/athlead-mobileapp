import React, {Component} from 'react';
import { StyleSheet, Platform, Image, Text, View, ActivityIndicator } from 'react-native';


class Loading extends Component {




  render() {

    return (
      <View style={styles.container}>
      <View style={{ height: 240, width: 300, backgroundColor: '#c5c6c7',
        alignItems: 'center', justifyContent: 'center', marginTop: 100}}>
          <Image source={require('./../mainDrawer/logo2.png')}
          style={{height: 180, width:220, borderRadius: 0}} />
        </View>

        <ActivityIndicator size='large' color='#263253' />
      </View>
    )
  }
}

export default Loading;

/*
<Text style={{fontSize: 26, color: '#3c3c3c'}}>
  Loading
</Text>
*/

const styles = StyleSheet.create({
  container: {
    //marginTop: '100px',
    flex: 1,
    //justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#c5c6c7',
  },
})
