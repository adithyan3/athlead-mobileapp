import React, {Component} from 'react';
import { StyleSheet, Text, TextInput, View, Button } from 'react-native';
import {auth, db} from './../../firebase';

const byPropKey = (propertyName, value) => () => ({
  [propertyName]: value,
});


class RegisterCoach extends Component {
  constructor(props) {
    super(props);

    this.state = {
      username: '',
      email: '',
      passwordOne: '',
      passwordTwo: '',
      teamCode: '',
      error: null,
    };
  }

  createActDB(name, email, tCode, id) {

    const teamRef = db.collection('teams').doc(tCode);
    var prog = '';
    teamRef.get().then((doc) => {
        const newUser = {
          name: name,
          email: email,
          organization: doc.data().organization,
          sport: doc.data().sport,
          //level: doc.data().level,
          seasonStart: doc.data().seasonStart,
          teamCode: tCode,
          type: 'coach',
          regDate: new Date(),
        }
        db.collection('users').doc(id).set(newUser)
        .then(() => {
          console.log('new user added');
        });
    })
  }


  handleReg = (e) => {

    const {
      username,
      email,
      passwordOne,
      teamCode,
    } = this.state;

    const teamRef = db.collection('teams').doc(teamCode);
    teamRef.get().then((doc) => {
      if (doc.exists) {

        auth.createUserWithEmailAndPassword(email, passwordOne)
          .then((authUser) => {
            this.createActDB(username, email, teamCode, authUser.user.uid);
            this.setState({
              username: '',
              email: '',
              passwordOne: '',
              passwordTwo: '',
              teamCode: '',
              error: null,
            });

          })
          .catch(error => {
            this.setState(byPropKey('error', error));
          });

    } else {
        //make notification to enter valid team code
    }}).catch(function(error) {
        console.log("Invalid team code", error);
    });


  };


  render() {
    const {
      username,
      email,
      passwordOne,
      passwordTwo,
      teamCode,
      error,
    } = this.state;

    const isInvalid =
        passwordOne !== passwordTwo ||
        passwordOne === '' ||
        email === '' ||
        username === '' ||
        teamCode === '';

    return (
      <View style={styles.container}>
        <Text style = {{fontSize: 20}}>Register with your provided team code</Text>
        {error &&
          <Text style={{ color: 'red' }}>
            {error.message}}
          </Text>}
        <TextInput
          placeholder="Full Name"
          autoCapitalize="none"
          style={styles.textInput}
          onChangeText={username => this.setState({ username })}
          value={username}
        />
        <TextInput
          placeholder="Email"
          autoCapitalize="none"
          style={styles.textInput}
          onChangeText={email => this.setState({ email })}
          value={email}
        />
        <TextInput
          secureTextEntry
          placeholder="Password"
          autoCapitalize="none"
          style={styles.textInput}
          onChangeText={passwordOne => this.setState({ passwordOne })}
          value={passwordOne}
        />
        <TextInput
          secureTextEntry
          placeholder="Confirm Password"
          autoCapitalize="none"
          style={styles.textInput}
          onChangeText={passwordTwo => this.setState({ passwordTwo })}
          value={passwordTwo}
        />
        <TextInput
          placeholder="Team Code"
          autoCapitalize="none"
          style={styles.textInput}
          onChangeText={teamCode => this.setState({ teamCode })}
          value={teamCode}
        />
        <View style = {styles.button}>
          <Button disabled={isInvalid} title="Register " onPress={this.handleReg} />
        </View>
        <View style = {styles.button}>
          <Button
            title="Already have an account? Login "
            onPress={() => this.props.navigation.navigate('Login')}
          />
        </View>


      </View>
    )
  }
}

export default RegisterCoach

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  textInput: {
    height: 40,
    width: '90%',
    borderColor: 'gray',
    borderWidth: 1,
    marginTop: 8
  },
  button: {
    paddingTop: 8
  }
})
