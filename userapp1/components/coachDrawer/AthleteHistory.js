import React, {Component} from 'react';
import { StyleSheet, Platform, Image, Text, View, Button } from 'react-native';
import { Container, Header, Content, Card, CardItem, Body, Title } from 'native-base';
import {auth,db} from './../../firebase';

class AthleteHistory extends Component {

  constructor(props) {
    super(props);

    this.state = {
      name: '',
      sport: '',
      level: '',
    };

  }

  static navigationOptions = {
    drawerLabel: 'AthleteHistory',
  };

  componentDidMount() {
    const athRef = db.collection('users').doc(this.props.uID);
    athRef.get().then((doc) => {
      this.setState({name: doc.data().name});
      this.setState({sport: doc.data().sport});
      this.setState({level: doc.data().level});
    });

  }

  render() {

    return (
      <Container>
        <Content>
          <Text>
            Athlete History {'\n'}
            User ID: {this.props.uID} {'\n'}
            Name: {this.state.name} {'\n'}
            Sport: {this.state.sport} {'\n'}
            Level: {this.state.level} {'\n'}
          </Text>
        </Content>
      </Container>


    )
  }
}

export default AthleteHistory;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
})
