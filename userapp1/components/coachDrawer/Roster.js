import React, {Component} from 'react';
import { StyleSheet, Platform, Image, View, TextInput, TouchableOpacity, FlatList, Text } from 'react-native';
import { Container, Header, Content, Card, CardItem, Body, Title } from 'native-base';
import {auth,db} from './../../firebase';

class Roster extends Component {


  static navigationOptions = {
    drawerLabel: 'Roster',
  };

  renderSeparator = () => {
    return (
      <View
        style={{
          height: 1,
          width: "100%",
          backgroundColor: "#CED0CE",
        }}
      />
    );
  };

  render() {

    const rost = this.props.screenProps.roster;
    console.log(rost);
    return (
      <Container>
        <Content>

          <Text>
            Roster for {this.props.screenProps.organization} , {this.props.screenProps.sport}
          </Text>

          <FlatList
            data={rost}
            renderItem={({item}) => (
              <TouchableOpacity onPress={() => this.props.navigation.navigate('AthleteHistory',
                {
                  uID: item.uID,
                })}>
                <Text style = {{marginLeft: 20, fontSize: 20, paddingTop: 5, paddingBottom: 5}}>{item.name}</Text>
              </TouchableOpacity>
            )}
            ItemSeparatorComponent={this.renderSeparator}
          />
        </Content>
      </Container>

    )
  }
}

export default Roster;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    width: '800px',
  }
})
