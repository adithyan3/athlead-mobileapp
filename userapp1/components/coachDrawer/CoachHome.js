import React, {Component} from 'react';
import { StyleSheet, Platform, Image, Text, View, Button } from 'react-native';
import { Container, Header, Content, Card, CardItem, Body, Title } from 'native-base';
import {auth,db} from './../../firebase';

class CoachHome extends Component {

  static navigationOptions = {
    drawerLabel: 'Home',
  };

  render() {
    return (
      <Container style = {styles.container}>
        <Content>
            <Text style = {styles.top}>
              Coach's Home Page {'\n'}
              Welcome Coach {this.props.screenProps.name}
            </Text>
            <Card>
              <CardItem body>
                <Text style = {styles.text}>
                  Organization: {this.props.screenProps.organization} {'\n'}
                  Sport: {this.props.screenProps.sport} {'\n'}
                </Text>
              </CardItem>
              <CardItem footer>
                <Button title = "View Roster" onPress={() => this.props.navigation.navigate('Roster')}>
                </Button>
                <Button title = "View Schedule" onPress={() => this.props.navigation.navigate('Schedule')}>
                </Button>
              </CardItem>
            </Card>

            </Content>
          </Container>
    )
  }
}

export default CoachHome;

const styles = StyleSheet.create({
  container: {
    //width: '600px',
    padding: 70,
    justifyContent: 'center',
    backgroundColor: '#c5c6c7',

  },
  top: {
    //margin: 24,
    fontSize: 24,
    fontWeight: 'bold',
    textAlign: 'center',
    color: '#3c3c3c',
  },
  text: {
    fontSize: 16,
    textAlign: 'left',
    color: '#3c3c3c',
  },
  footer: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  }
})
