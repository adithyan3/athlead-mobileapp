import React, {Component} from 'react';
import { StyleSheet, Platform, Image, Text, View, Button } from 'react-native';
import { Container, Header, Content, Card, CardItem, Body, Title } from 'native-base';
import {auth,db} from './../../firebase';

class Schedule extends Component {

  static navigationOptions = {
    drawerLabel: 'Schedule',
  };

  render() {

    return (
      <View style = {styles.container}>

          <Text>
            Schedule - Coming Soon!
          </Text>

      </View>

    )
  }
}

export default Schedule;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
})
