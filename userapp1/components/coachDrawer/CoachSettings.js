import React, {Component} from 'react';
import { StyleSheet, Platform, Image, Text, View, Button } from 'react-native';
import { Container, Header, Content, Card, CardItem, Body, Title } from 'native-base';
import {auth,db} from './../../firebase';

class CoachSettings extends Component {


  static navigationOptions = {
    drawerLabel: 'Settings',
  };

  handleLogout = (e) => {
      auth.signOut()
      .then(() => {this.props.navigation.navigate('Welcome')})
  }

  render() {

    return (
      <Container>
        <Content>
          <Text>
            Coach Settings - Coming Soon! {'\n'}
          </Text>
          <Button
            color='#fe1a27'
            title="Log Out "
            onPress={this.handleLogout}
          />

        </Content>
      </Container>

    )
  }
}

export default CoachSettings;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
})
