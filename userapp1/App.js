import React, {Component} from 'react';
import { View, Text, ActivityIndicator, StyleSheet, YellowBox } from 'react-native';
import {Auth, DrawNav} from './router';
import {CoachDrawNav} from './coachRouter';

import setUpSession from './setUpSession';

import UserContext from './UserContext'

import Loading from './components/login/Loading'


YellowBox.ignoreWarnings(['Warning: isMounted(...) is deprecated', 'Module RCTImageLoader']);
YellowBox.ignoreWarnings(['Setting a timer']);


class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      userID: '',
    }
  }

  setUser = (id) => {
    this.setState({userID: id});
  }

  render() {
    return (
      <UserContext.Consumer>
        {authUser => authUser.authUser
          ? (authUser.loaded ?
            ((authUser.type === 'coach') ?
            <CoachDrawNav
            screenProps={{
              ...authUser}
            } /> :
            <DrawNav
            screenProps={{
              ...authUser}
            } />) : <Loading />)
          : <Auth />
        }

      </UserContext.Consumer>

    )
  }
}

export default setUpSession(App);
