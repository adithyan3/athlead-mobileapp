import React, {Component} from 'react';
import { SafeAreaView, StyleSheet, Platform, Image, Text, View,
  Button, Dimensions, ScrollView } from 'react-native';
import { createSwitchNavigator, createStackNavigator,
  createDrawerNavigator, createMaterialTopTabNavigator, DrawerItems } from 'react-navigation';

import Welcome from './components/login/Welcome';
import Register from './components/login/Register';
import RegisterTeamAth from './components/login/RegisterTeamAth';
import RegisterCoach from './components/login/RegisterCoach';
import RegisterIndivAth from './components/login/RegisterIndivAth';
import Login from './components/login/Login';
import ForgotPass from './components/login/ForgotPass';

import Loading from './components/login/Loading';

import Home from './components/mainDrawer/Home';
import Exercises from './components/mainDrawer/exercise/Exercises';
import ExerciseView from './components/mainDrawer/exercise/ExerciseView';
import Program from './components/mainDrawer/workout/Program';
import Preview from './components/mainDrawer/workout/Preview';
import Record from './components/mainDrawer/workout/Record';
import Profile from './components/mainDrawer/Profile';

import Settings from './components/mainDrawer/Settings';
import Contact  from './components/mainDrawer/Contact';

import History from './components/mainDrawer/progress/History';
import WorkoutHist from './components/mainDrawer/progress/WorkoutHist';
import LogOut from './components/LogOut'

import Ionicons from 'react-native-vector-icons/Ionicons';



const mapNavigationStateParamsToProps = (SomeComponent) => {
    return class extends Component {
        static navigationOptions = SomeComponent.navigationOptions;
        render() {
            const {navigation: {state: {params}}} = this.props
            return <SomeComponent {...params} {...this.props} />
        }
    }
}

export const ProgramStack = createStackNavigator({
    Program: {
      screen: Program,
    },

  },
  {
    navigationOptions: {
      gesturesEnabled: false,
      header: null,
    },
    initialRouteName: 'Program'
  },

)

export const TodayStack = createStackNavigator({
    Home: {
      screen: Home,
    },

  },
  {
    navigationOptions: {
      gesturesEnabled: false,
      header: null,
    },
    initialRouteName: 'Home'
  },
)

export const WorkoutTabNav = createMaterialTopTabNavigator({
  Home: {
    screen: TodayStack,
    navigationOptions: {
      tabBarLabel: 'Todays Workout ',
    }
  },
  Program: {
    screen: ProgramStack,
    navigationOptions: {
      tabBarLabel: 'Your Program ',
    }
  },
},
{
  tabBarOptions: {
    style: {
      backgroundColor: '#324151',
    },
    activeTintColor: '#fe1a27',
    inactiveTintColor: '#fff',
    labelStyle: {
      fontFamily: 'Montserrat-Regular',
      fontSize: 16,
    },

    indicatorStyle: {
      backgroundColor: '#fe1a27'
    }
  },
  swipeEnabled: true,
  initialRouteName: 'Home',
});

export const WorkoutStack= createStackNavigator(
  {
    TabNavigator:{
      screen: WorkoutTabNav,
      navigationOptions: ({ navigation }) => ({
        drawerLabel: 'Workout ',
        headerTitle: 'Workout ',
        headerMode: 'screen',
        headerStyle: {
          backgroundColor: '#324151',
        },
        headerTintColor: '#c5c6c7',
        headerTitleStyle: {
          fontWeight: 'bold',
        },
        headerLeft: <Ionicons name="md-menu" style={{padding:10}}
          size={35} color='#c5c6c7' onPress={() => navigation.openDrawer()} />
      })
    },
    Preview: {
      screen: mapNavigationStateParamsToProps(Preview),
      navigationOptions: ({navigation}) => ({
        headerTitle: 'Preview ',
        headerMode: 'screen',
        headerStyle: {
          backgroundColor: '#324151',
        },
        headerTintColor: '#c5c6c7',
        headerTitleStyle: {
          fontWeight: 'bold',
        },
        //headerLeft: <Ionicons name="md-menu" style={{padding:10}}
          //size={35} color='#c5c6c7' onPress={() => navigation.openDrawer()} />
      })
    },
    Record: {
      screen: mapNavigationStateParamsToProps(Record),
      navigationOptions: ({navigation}) => ({
        headerTitle: 'Record ',
        headerMode: 'screen',
        headerStyle: {
          backgroundColor: '#324151',
        },
        headerTintColor: '#c5c6c7',
        headerTitleStyle: {
          fontWeight: 'bold',
        },
        //headerLeft: <Ionicons name="md-menu" style={{padding:10}}
          //size={35} color='#c5c6c7' onPress={() => navigation.openDrawer()} />
      })
    },
  },
  {
    initialRouteName: 'TabNavigator',
  }
);

export const ProgTabNav = createMaterialTopTabNavigator({
  History: {
    screen: History,
    navigationOptions: {
      tabBarLabel: 'Strength Log ',
    }
  },
  WorkoutHist: {
    screen: WorkoutHist,
    navigationOptions: {
      tabBarLabel: 'Workout History',
    }
  },
},
{
  tabBarOptions: {
    style: {backgroundColor: '#324151'},
    activeTintColor: '#fe1a27',
    inactiveTintColor: '#fff',
    labelStyle: {
      fontFamily: 'Montserrat-Regular',
      fontSize: 16,
    },

    indicatorStyle: {
      backgroundColor: '#fe1a27'
    }
  },
  swipeEnabled: true,
  initialRouteName: 'History',
  navigationOptions: {
    gesturesEnabled: false,
  },
});


export const ProgressStack = createStackNavigator({
  TabNavigator:{
    screen: ProgTabNav,
    navigationOptions: ({ navigation }) => ({
      drawerLabel: 'Progress ',
      headerTitle: 'Progress ',
      headerMode: 'screen',
      headerStyle: {
        backgroundColor: '#324151',
      },
      headerTintColor: '#c5c6c7',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
      headerLeft: <Ionicons name="md-menu" style={{padding:10}}
        size={35} color='#c5c6c7' onPress={() => navigation.openDrawer()} />
    })
  }
});

export const ExerciseStack = createStackNavigator(
  {
    Exercises: {
      screen: Exercises,
    },
    ExerciseView: {
      screen: mapNavigationStateParamsToProps(ExerciseView),
    }
  },
  {
    navigationOptions: ({navigation}) => ({
      initialRouteName: 'Exercises ',
      drawerLabel: 'Exercises Guide ',
      headerMode: 'screen',
      headerTitle: 'Exercise Guide ',
      headerStyle: {
        backgroundColor: '#324151',
      },
      headerTintColor: '#c5c6c7',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
      headerLeft: <Ionicons name="md-menu" style={{padding:10}}
        size={35} color='#c5c6c7' onPress={() => navigation.openDrawer()} />
    }),
  }
);

handleLogout = (e) => {
    auth.signOut()
    .then(() => {this.props.navigation.navigate('Welcome')})
}

const {width} = Dimensions.get('window');

export const CustomDrawComp = (props) => (
  <SafeAreaView style={{flex:1}}>
    <View style={{ height: 220, backgroundColor: '#bfbfbf',
      alignItems: 'center', justifyContent: 'center'}}>
      <Image source={require('./components/mainDrawer/logo2.png')}
      style={{height: 180, width:220, borderRadius: 0}} />
    </View>

    <ScrollView style={{marginBottom: 0, backgroundColor: '#bfbfbf'}}>
      <DrawerItems {...props} />
      <LogOut />
    </ScrollView>

  </SafeAreaView>
)


export const ProfileStack = createStackNavigator(
  {
    Profile: {
      screen: Profile,
    },
  },
  {
    navigationOptions: ({navigation}) => ({
      initialRouteName: 'Profile ',
      drawerLabel: 'Profile ',
      headerMode: 'screen',
      headerTitle: 'Profile ',
      headerStyle: {
        backgroundColor: '#324151',
      },
      headerTintColor: '#c5c6c7',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
      headerLeft: <Ionicons name="md-menu" style={{padding:10}}
        size={35} color='#c5c6c7' onPress={() => navigation.openDrawer()} />
    }),
  }
);

export const ContactStack = createStackNavigator(
  {
    Contact: {
      screen: Contact,
    },
  },
  {
    navigationOptions: ({navigation}) => ({
      initialRouteName: 'Contact ',
      drawerLabel: 'Contact ',
      headerMode: 'screen',
      headerTitle: 'Contact ',
      headerStyle: {
        backgroundColor: '#324151',
      },
      headerTintColor: '#c5c6c7',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
      headerLeft: <Ionicons name="md-menu" style={{padding:10}}
        size={35} color='#c5c6c7' onPress={() => navigation.openDrawer()} />
    }),
  }
);



export const DrawNav = createDrawerNavigator({
  Workout: WorkoutStack,
  Progress: ProgressStack,
  Exercises: ExerciseStack,
  Profile: ProfileStack,
  //Settings: Settings,
  //Contact: ContactStack,
},
{
  contentComponent: CustomDrawComp,
  drawerWidth: width*.65,
  contentOptions: {
      activeTintColor: '#fe1a27',
      labelStyle: {fontFamily: 'Montserrat-Regular', fontSize: 16},
    }
});

export const RegStack = createStackNavigator(
  {
    Register: {
      screen: Register,
    },
    RegisterTeamAth: {
      screen: RegisterTeamAth,
    },
    RegisterCoach: {
      screen: RegisterCoach,
    },
    RegisterIndivAth: {
      screen: RegisterIndivAth,
    },
  },
  {
    navigationOptions: {
      gesturesEnabled: false,
      header: null,
    },
    initialRouteName: 'Register'
  },

)


export const Auth = createStackNavigator(
  {
    Welcome: {
      screen: Welcome,
    },
    RegStack: {
      screen: RegStack,
    },
    Login: {
      screen: Login,
    },
    ForgotPass: {
      screen: ForgotPass,
    },
    Loading: {
      screen: Loading,
    },
    /*DrawNav: {
      screen: DrawNav,
    },*/

  },
  {
    navigationOptions: {
      gesturesEnabled: false,
      header: null,
    },
    initialRouteName: 'Welcome'
  },
);

/*
export const PreviewStack = createStackNavigator({
    Workout: {
      screen: mapNavigationStateParamsToProps(Workout),
    },
  },
  {
    navigationOptions: ({navigation}) => ({
      initialRouteName: 'Workout ',
      headerMode: 'screen',
      headerTitle: 'Workout Preview ',
      headerStyle: {
        backgroundColor: '#324151',
      },
      backBehavior: DrawNav,
      headerTintColor: '#c5c6c7',
      headerTitleStyle: {
        fontSize: 18,
      },
      headerLeft: <Ionicons name="ios-arrow-back" style={{padding:10}}
        size={35} color='#c5c6c7' onPress={() => navigation.navigate('DrawNav')} />
    }),
  }
)*/
