import React, {Component} from 'react';
import { SafeAreaView, StyleSheet, Platform, Image, Text, View,
  Button, Dimensions, ScrollView } from 'react-native';
import { createSwitchNavigator, createStackNavigator,
  createDrawerNavigator, createMaterialTopTabNavigator, DrawerItems } from 'react-navigation';


import Welcome from './components/login/Welcome';
import Register from './components/login/Register';
import RegisterTeamAth from './components/login/RegisterTeamAth';
import RegisterCoach from './components/login/RegisterCoach';
import RegisterIndivAth from './components/login/RegisterIndivAth';
import Login from './components/login/Login';
import ForgotPass from './components/login/ForgotPass';
import Loading from './components/login/Loading';

import CoachHome from './components/coachDrawer/CoachHome';

import Exercises from './components/mainDrawer/exercise/Exercises';
import ExerciseView from './components/mainDrawer/exercise/ExerciseView';

import Roster from './components/coachDrawer/Roster';
import CoachSettings from './components/coachDrawer/CoachSettings';
import Schedule from './components/coachDrawer/Schedule';

import AthleteHistory from './components/coachDrawer/AthleteHistory';

import LogOut from './components/LogOut';


import Ionicons from 'react-native-vector-icons/Ionicons';



const mapNavigationStateParamsToProps = (SomeComponent) => {
    return class extends Component {
        static navigationOptions = SomeComponent.navigationOptions;
        render() {
            const {navigation: {state: {params}}} = this.props
            return <SomeComponent {...params} {...this.props} />
        }
    }
}

export const RosterStack = createStackNavigator(
  {
    Roster: {
      screen: Roster,
    },
    AthleteHistory: {
      screen: mapNavigationStateParamsToProps(AthleteHistory),
    }
  },
  {
    navigationOptions: ({navigation}) => ({
      initialRouteName: 'Roster',
      drawerLabel: 'Roster',
      headerMode: 'screen',
      headerTitle: 'Roster ',
      headerStyle: {
        backgroundColor: '#324151',
      },
      headerTintColor: '#c5c6c7',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
      headerLeft: <Ionicons name="md-menu" style={{padding:10}}
        size={35} color='#c5c6c7' onPress={() => navigation.openDrawer()} />
    }),
  }
);

export const CoachHomeStack = createStackNavigator({
    CoachHome: {
      screen: CoachHome,
    },
    Roster: {
      screen: RosterStack,
      navigationOptions: {
        header: null
      }
    },
    Schedule: {
      screen: Schedule,
    },
  },
  {
    navigationOptions: ({navigation}) => ({
      initialRouteName: 'CoachHome ',
      drawerLabel: 'Home ',
      headerMode: 'screen',
      headerTitle: 'Coach Home',
      headerStyle: {
        backgroundColor: '#324151',
      },
      headerTintColor: '#c5c6c7',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
      headerLeft: <Ionicons name="md-menu" style={{padding:10}}
        size={35} color='#c5c6c7' onPress={() => navigation.openDrawer()} />
    }),
  }
)




export const ExerciseStack = createStackNavigator(
  {
    Exercises: {
      screen: Exercises,
    },
    ExerciseView: {
      screen: mapNavigationStateParamsToProps(ExerciseView),
    }
  },
  {
    navigationOptions: ({navigation}) => ({
      initialRouteName: 'Exercises ',
      drawerLabel: 'Exercises Guide ',
      headerMode: 'screen',
      headerTitle: 'Exercise Guide ',
      headerStyle: {
        backgroundColor: '#324151',
      },
      headerTintColor: '#c5c6c7',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
      headerLeft: <Ionicons name="md-menu" style={{padding:10}}
        size={35} color='#c5c6c7' onPress={() => navigation.openDrawer()} />
    }),
  }
);



const {width} = Dimensions.get('window');

export const CustomDrawComp = (props) => (
  <SafeAreaView style={{flex:1}}>
    <View style={{ height: 220, backgroundColor: '#bfbfbf',
      alignItems: 'center', justifyContent: 'center'}}>
      <Image source={require('./components/mainDrawer/logo2.png')}
      style={{height: 180, width:220, borderRadius: 0}} />
    </View>

    <ScrollView style={{marginBottom: 0, backgroundColor: '#bfbfbf'}}>
      <DrawerItems {...props} />
      <LogOut />
    </ScrollView>

  </SafeAreaView>
)

export const SettingsStack = createStackNavigator(
  {
    CoachSettings: {
      screen: CoachSettings,
    },
  },
  {
    navigationOptions: ({navigation}) => ({
      initialRouteName: 'CoachSettings ',
      drawerLabel: 'Settings ',
      headerMode: 'screen',
      headerTitle: 'Settings ',
      headerStyle: {
        backgroundColor: '#324151',
      },
      headerTintColor: '#c5c6c7',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
      headerLeft: <Ionicons name="md-menu" style={{padding:10}}
        size={35} color='#c5c6c7' onPress={() => navigation.openDrawer()} />
    }),
  }
);



export const CoachDrawNav = createDrawerNavigator({
  Home: CoachHomeStack,
  Roster: RosterStack,
  Exercises: ExerciseStack,
  Settings: SettingsStack,
  //Contact: Contact,
},
{
  contentComponent: CustomDrawComp,
  drawerWidth: width*.65,
  contentOptions: {
      activeTintColor: '#fe1a27',
      labelStyle: {fontFamily: 'Montserrat-Regular', fontSize: 16},
    }
});

export const RegStack = createStackNavigator(
  {
    Register: {
      screen: Register,
    },
    RegisterTeamAth: {
      screen: RegisterTeamAth,
    },
    RegisterCoach: {
      screen: RegisterCoach,
    },
    RegisterIndivAth: {
      screen: RegisterIndivAth,
    },
  },
  {
    navigationOptions: {
      gesturesEnabled: false,
      header: null,
    },
    initialRouteName: 'Register'
  },

)


export const Auth = createStackNavigator(
  {
    Welcome: {
      screen: Welcome,
    },
    RegStack: {
      screen: RegStack,
    },
    Login: {
      screen: Login,
    },
    ForgotPass: {
      screen: ForgotPass,
    },
    Loading: {
      screen: Loading,
    },
    /*CoachDrawNav: {
      screen: CoachDrawNav,
    },*/

  },
  {
    navigationOptions: {
      gesturesEnabled: false,
      header: null,
    },
    initialRouteName: 'Welcome'
  },
);

/*
export const PreviewStack = createStackNavigator({
    Workout: {
      screen: mapNavigationStateParamsToProps(Workout),
    },
  },
  {
    navigationOptions: ({navigation}) => ({
      initialRouteName: 'Workout ',
      headerMode: 'screen',
      headerTitle: 'Workout Preview ',
      headerStyle: {
        backgroundColor: '#324151',
      },
      backBehavior: CoachDrawNav,
      headerTintColor: '#c5c6c7',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
      headerLeft: <Ionicons name="ios-arrow-back" style={{padding:10}}
        size={35} color='#c5c6c7' onPress={() => navigation.navigate('CoachDrawNav')} />
    }),
  }
)*/
